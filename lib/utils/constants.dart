
const SECRET_NAME = "Aztorg";
const APP_TYPE = "client";
const BASE_BDM_URL = "http://isti.uz/mobiles/load_config/";
const BASE_URL = "http://api.ilm-izlab.uz/api/";
const BASE_IMAGE_URL = "http://api.ilm-izlab.uz/";

const RU_LANG_KEY = "ru";
const KZ_LANG_KEY = "kk";
const DEFAULT_LANG_KEY = RU_LANG_KEY;
const LANG_SELECTED = "lang_selected";

const EVENT_UPDATE_CART = 1;
