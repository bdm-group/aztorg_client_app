

import 'dart:ui';

import '../extensions/extensions.dart';


class AppColors{
  static Color RED_COLOR = HexColor.fromHex("#B41D22");
  static Color LIGHT_RED_COLOR = HexColor.fromHex("#E4B8BC");

  static Color YELLOW_COLOR = HexColor.fromHex("#FCCC28");
  static Color BLACK_COLOR = HexColor.fromHex("#1B1B1B");
  static Color GREY_TEXT_COLOR = HexColor.fromHex("#9098B1");
  static Color GREY_LIGHT_COLOR = HexColor.fromHex("#EBF0FF");
  static Color GREY_COLOR = HexColor.fromHex("#9098B1");
  static Color BACKGROUND_COLOR = HexColor.fromHex("#F9FAFE");


  static List<Color> RANDOM_COLORS = [
    HexColor.fromHex("#F8A44C"),
    HexColor.fromHex("#53B175"),
    HexColor.fromHex("#D3B0E0"),
    HexColor.fromHex("#CD6ECD"),
    HexColor.fromHex("#B7DFF5"),
    HexColor.fromHex("#FDE598"),
  ];
}