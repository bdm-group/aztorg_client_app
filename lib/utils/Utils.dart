


import 'package:flutter/material.dart';

class Utils {

}

void showAlert(BuildContext context, String text) {
  var alert = AlertDialog(
    content: Container(
      child: Row(
        children: <Widget>[Text(text)],
      ),
    ),
    actions: <Widget>[
      FlatButton(
          onPressed: () => Navigator.pop(context),
          child: const Text(
            "OK",
            style: TextStyle(color: Colors.white),
          ))
    ],
  );
}

void hideKeyboard(BuildContext context) {
FocusScope.of(context).requestFocus(FocusNode());
}