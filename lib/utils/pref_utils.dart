import 'dart:convert';

import 'package:aztorg_flutter/model/basket_model.dart';
import 'package:aztorg_flutter/model/bdm_items_model.dart';
import 'package:aztorg_flutter/model/branch_model.dart';
import 'package:aztorg_flutter/model/category_model.dart';
import 'package:aztorg_flutter/model/event_model.dart';
import 'package:aztorg_flutter/model/product_model.dart';
import 'package:event_bus/event_bus.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../model/response/login_response.dart';
import 'constants.dart';

class PrefUtils {
  static SharedPreferences? _singleton;

  static const PREF_BDM_ITEMS = "bdm_items";
  static const PREF_SELECTED_BRANCH = "selected_branch";
  static const PREF_FAVORITES = "favorites";
  static const PREF_BASE_URL = "base_url";
  static const PREF_BASE_IMAGE_URL = "base_image_url";
  static const PREF_FCM_TOKEN = "fcm_token";
  static const PREF_TOKEN = "token";
  static const PREF_CATEGORY_LIST = "category_list";
  static const PREF_BASKET = "basket";
  static const PREF_USER = "user";
  static const PREF_LANG = "lang";

  static SharedPreferences? getInstance() {
    return _singleton;
  }

  static initInstance() async {
    _singleton = await SharedPreferences.getInstance();
  }

  static BdmItemsModel? getBdmItems() {
    var json = _singleton?.getString(PREF_BDM_ITEMS);
    return json == null ? null : BdmItemsModel.fromJson(jsonDecode(json));
  }

  static Future<bool> setBdmItems(BdmItemsModel items) async {
    return _singleton!.setString(PREF_BDM_ITEMS, jsonEncode(items.toJson()));
  }

  static String getBaseUrl() {
    return _singleton?.getString(PREF_BASE_URL) ?? "";
  }

  static Future<bool> setBaseUrl(String value) async {
    return _singleton!.setString(PREF_BASE_URL, value);
  }

  static String getBaseImageUrl() {
    return _singleton?.getString(PREF_BASE_IMAGE_URL) ?? "";
  }

  static Future<bool> setBaseImageUrl(String value) async {
    return _singleton!.setString(PREF_BASE_IMAGE_URL, value);
  }

  static String getFCMToken() {
    return _singleton?.getString(PREF_FCM_TOKEN) ?? "";
  }

  static Future<bool> setFCMToken(String value) async {
    return _singleton!.setString(PREF_FCM_TOKEN, value);
  }

  static String getToken() {
    return _singleton?.getString(PREF_TOKEN) ?? "";
  }

  static Future<bool> setToken(String value) async {
    return _singleton!.setString(PREF_TOKEN, value);
  }

  static BranchModel? getSelectedBranch() {
    var json = _singleton?.getString(PREF_SELECTED_BRANCH);
    return json == null ? null : BranchModel.fromJson(jsonDecode(json));
  }

  static Future<bool> setSelectedBranch(BranchModel value) async {
    return _singleton!
        .setString(PREF_SELECTED_BRANCH, jsonEncode(value.toJson()));
  }

  static List<String> getFavoriteList() {
    return _singleton!.getStringList(PREF_FAVORITES) ?? [];
  }

  static Future<bool> setFavoriteProduct(ProductModel item) async {
    var items = getFavoriteList();
    if (items.where((element) => item.id == element).isNotEmpty) {
      items = items.where((element) => element != item.id).toList();
    } else {
      items.add(item.id);
    }
    return _singleton!.setStringList(PREF_FAVORITES, items);
  }

  static List<CategoryModel> getCategoryList(
      {String parentId = "00000000-0000-0000-0000-000000000000"}) {
    var json = _singleton?.getString(PREF_CATEGORY_LIST);
    if (json == null) {
      return [];
    }
    var items = (jsonDecode(json) as List<dynamic>)
        .map((js) => CategoryModel.fromJson(js));
    return items.where((element) => element.category_id == parentId).toList();
  }

  static List<CategoryModel> getParentCategoryList(String parentId) {
    var json = _singleton?.getString(PREF_CATEGORY_LIST);
    if (json == null) {
      return [];
    }
    var items = (jsonDecode(json) as List<dynamic>)
        .map((js) => CategoryModel.fromJson(js));
    return items.where((element) => element.id == parentId).toList();
  }

  static Future<bool> setCategoryList(List<CategoryModel> value) async {
    return _singleton!.setString(PREF_CATEGORY_LIST,
        jsonEncode(value.map((item) => item.toJson()).toList()));
  }

  static Future<bool> clearCart() async {
    var result = await _singleton!.setString(PREF_BASKET, jsonEncode([].toList()));
    EventBus().fire(EventModel(EVENT_UPDATE_CART, true));
    return result;
  }

  static List<BasketModel> getCartList() {
    var json = _singleton?.getString(PREF_BASKET);
    if (json == null) {
      return [];
    }
    var items = (jsonDecode(json) as List<dynamic>)
        .map((js) => BasketModel.fromJson(js));
    return items.toList();
  }

  static double getCartCount(String id) {
    final items =
        PrefUtils.getCartList().where((element) => element.id == id).toList();
    return items.isNotEmpty ? items.first.count : 0.0;
  }

  static Future<bool> addToCart(BasketModel item) async {
    var items = getCartList();

    if (item.count == 0) {
      var index = 0;
      var removeIndex = -1;

      for (var element in items) {
        if (element.id == item.id) {
          removeIndex = index;
        }
        index++;
      }
      if (removeIndex > -1) {
        items.removeAt(removeIndex);
      }
    } else {
      var isHave = false;
      for (var element in items) {
        if (element.id == item.id) {
          element.count = item.count;
          isHave = true;
        }
      }

      if (!isHave) {
        items.add(item);
      }
    }

    var r = await _singleton!.setString(
        PREF_BASKET, jsonEncode(items.map((item) => item.toJson()).toList()));
    EventBus().fire(EventModel(EVENT_UPDATE_CART, true));
    return r;
  }

  static LoginResponse getUser() {
    return LoginResponse.fromJson(
        jsonDecode(_singleton?.getString(PREF_USER) ?? ""));
  }

  static Future<bool> setUser(LoginResponse value) async {
    return _singleton!.setString(PREF_USER, jsonEncode(value.toJson()));
  }

  static String getLang() {
    return _singleton?.getString(PREF_LANG) ?? DEFAULT_LANG_KEY;
  }

  static Future<bool> setLang(String value) async {
    return _singleton!.setString(PREF_LANG, value);
  }


  static bool isLangSelected() {
    return _singleton?.getBool(LANG_SELECTED) ?? false;
  }

  static Future<bool> setLangSelected(bool value) async {
    return _singleton!.setBool(LANG_SELECTED, value);
  }

  static void clearAll() async {
    final pref = await SharedPreferences.getInstance();
    await pref.clear();
  }
}
