import 'package:alice/alice.dart';
import 'package:aztorg_flutter/screen/main/report/report_screen.dart';
import 'package:aztorg_flutter/screen/splash/splash_screen.dart';
import 'package:aztorg_flutter/utils/constants.dart';
import 'package:aztorg_flutter/utils/pref_utils.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';

import 'utils/app_colors.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // await Firebase.initializeApp();
  await EasyLocalization.ensureInitialized();
  runApp(
      EasyLocalization(
      child: const MyApp(),
      supportedLocales: [Locale(RU_LANG_KEY), Locale(KZ_LANG_KEY)],
      fallbackLocale: Locale(DEFAULT_LANG_KEY),
      path: "assets/translations"));
}

class MyApp extends StatefulWidget {
  static final alice = Alice(showNotification: kDebugMode);

  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    setupInteractedMessage();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    context.setLocale(Locale(PrefUtils.getLang()));
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      navigatorKey: MyApp.alice.getNavigatorKey(),
      title: 'Aztorg',
      theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          textTheme: TextTheme(
            headlineMedium: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
                color: AppColors.BLACK_COLOR),
            headlineLarge: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                color: AppColors.BLACK_COLOR),
            bodyMedium: TextStyle(fontSize: 16, color: AppColors.BLACK_COLOR),
            bodySmall:
                TextStyle(fontSize: 12, color: AppColors.GREY_TEXT_COLOR),
          ),
          primaryColor: Colors.white,
          primaryColorDark: AppColors.BLACK_COLOR,
          backgroundColor: AppColors.BACKGROUND_COLOR,
          primarySwatch: Colors.yellow,
          elevatedButtonTheme: ElevatedButtonThemeData(
            style: TextButton.styleFrom(
                backgroundColor: AppColors.YELLOW_COLOR,
                padding: EdgeInsets.all(16),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5))),
          ),
          appBarTheme: AppBarTheme(color: Colors.white, elevation: 1),
          primaryIconTheme:
              IconThemeData(color: AppColors.RED_COLOR, size: 16)),
      home: SplashScreen(),
    );
  }

  Future<void> setupInteractedMessage() async {
    // Get any messages which caused the application to open from
    // a terminated state.
    RemoteMessage? initialMessage =
    await FirebaseMessaging.instance.getInitialMessage();

    NotificationSettings settings =
    await FirebaseMessaging.instance.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );
    await FirebaseMessaging.instance.subscribeToTopic("news");
    var token = await FirebaseMessaging.instance.getToken();
    PrefUtils.setFCMToken(token ?? "");
    print("TOKEN: $token");
    // If the message also contains a data property with a "type" of "chat",
    // navigate to a chat screen
    if (initialMessage != null) {
      _handleMessage(initialMessage);
    }
    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

    // Also handle any interaction when the app is in the background via a
    // Stream listener
    FirebaseMessaging.onMessageOpenedApp.listen(_handleMessage);
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      print("message: ${message}");
      RemoteNotification notification = message.notification!;
      AndroidNotification android = message.notification!.android!;

      // If `onMessage` is triggered with a notification, construct our own
      // local notification to show to users using the created channel.
      if (notification != null && android != null) {
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails("channel.id", "channel.name",
                  icon: "@mipmap/launcher_icon"
                // other properties...
              ),
            ));
      }
    });
  }

  void _handleMessage(RemoteMessage message) {
    print("handler: " + message.toString());
  }
}
