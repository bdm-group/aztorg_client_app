import 'package:aztorg_flutter/screen/auth/auth_viewmodel.dart';
import 'package:aztorg_flutter/utils/app_colors.dart';
import 'package:aztorg_flutter/view/custom_views.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:stacked/stacked.dart';

import '../main/main_screen.dart';
import 'package:easy_localization/easy_localization.dart';

enum AuthState { phone, sms_code, registration }

class AuthScreen extends StatefulWidget {
  const AuthScreen({Key? key}) : super(key: key);

  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  var state = AuthState.phone;
  var activeButton = false;
  var phoneController = TextEditingController(text: "+7 ");
  var fullnameController = TextEditingController();
  var confirmCodeController = TextEditingController();

  var phoneFormatter = MaskTextInputFormatter(mask: '+7 ### ### ## ##.');
  // var phoneFormatter = MaskTextInputFormatter(mask: '+998 (##) ### ## ##');
  var passwordFormatter = MaskTextInputFormatter(mask: '######');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: ViewModelBuilder<AuthViewModel>.reactive(
          viewModelBuilder: () {
            return AuthViewModel();
          },
          builder: (context, viewModel, child) {
            return Container(
              padding: EdgeInsets.all(16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Center(
                    child: Container(
                      margin: EdgeInsets.only(top: 100),
                      height: 72,
                      width: 72,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(16),
                          color: AppColors.RED_COLOR),
                      child: Center(
                        child: Image.asset(
                          "assets/images/ellipse.png",
                          width: 44,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  Text(
                    "welcome_to_aztorg".tr(),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.headlineMedium,
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  Text(
                    "login_for_continue".tr(),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.bodySmall,
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  CustomViews.buildTextField(
                      "phone_number".tr(), "phone_number_".tr(),
                      controller: phoneController,
                      inputType: TextInputType.phone,
                      maskTextInputFormatter: phoneFormatter,
                      enabled: state == AuthState.phone, onChanged: (text) {
                    setState(() {
                      activeButton = phoneController.text.length > 10;
                    });
                  }),
                  if (state != AuthState.phone)
                    const SizedBox(
                      height: 16,
                    ),
                  if (state == AuthState.registration)
                    CustomViews.buildTextField("name".tr(), "name_".tr(),
                        controller: fullnameController,
                        inputType: TextInputType.name),
                  if (state == AuthState.registration)
                    const SizedBox(
                      height: 16,
                    ),
                  if (state != AuthState.phone)
                    CustomViews.buildTextField(
                        "sms_confirm".tr(), "sms_confirm_".tr(),
                        controller: confirmCodeController,
                        maskTextInputFormatter: passwordFormatter,
                        inputType: TextInputType.number),
                  const SizedBox(
                    height: 32,
                  ),
                  viewModel.progressData
                      ? const Center(
                          child: CircularProgressIndicator(),
                        )
                      : ElevatedButton(
                          onPressed: () {
                            if (activeButton) {
                              if (state == AuthState.phone) {
                                viewModel.smsCheck(phoneController.text
                                    .replaceAll(" ", "")
                                    .replaceAll("(", "")
                                    .replaceAll(")", ""));
                              } else if (state == AuthState.registration) {
                                viewModel.regestry(
                                    phoneController.text
                                        .replaceAll(" ", "")
                                        .replaceAll("(", "")
                                        .replaceAll(")", ""),
                                    fullnameController.text,
                                    confirmCodeController.text);
                              } else {
                                viewModel.regestry(
                                    phoneController.text
                                        .replaceAll(" ", "")
                                        .replaceAll("(", "")
                                        .replaceAll(")", ""),
                                    "name",
                                    confirmCodeController.text);
                              }
                            }
                          },
                          child: Text(state == AuthState.phone
                              ? "sign".tr()
                              : "confirm".tr())),
                ],
              ),
            );
          },
          onModelReady: (viewModel) {
            viewModel.errorData.listen((event) {
              ScaffoldMessenger.maybeOf(context)
                  ?.showSnackBar(SnackBar(content: Text(event)));
            });

            viewModel.smsCheckData.listen((event) {
              setState(() {
                state = event ? AuthState.sms_code : AuthState.registration;
              });
            });

            viewModel.loginResponseData.listen((event) {
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (_) => MainScreen()),
                  (route) => false);
            });
          },
        ),
      ),
    );
  }
}
