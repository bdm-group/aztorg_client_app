import 'dart:async';

import 'package:stacked/stacked.dart';

import '../../api/api_service.dart';
import '../../model/response/login_response.dart';
import '../../utils/pref_utils.dart';

class AuthViewModel extends BaseViewModel {
  final api = ApiService();

  StreamController<String> _errorStream = StreamController();

  Stream<String> get errorData {
    return _errorStream.stream;
  }

  var progressData = false;

  StreamController<bool> _smsCheckPhoneStream = StreamController();
  StreamController<LoginResponse> _loginResponseStream = StreamController();

  Stream<bool> get smsCheckData {
    return _smsCheckPhoneStream.stream;
  }

  Stream<LoginResponse> get loginResponseData {
    return _loginResponseStream.stream;
  }

  StreamController<String> tokenStream = StreamController();

  Stream<String> get tokenData {
    return tokenStream.stream;
  }

  void smsCheck(String phone) async {
    progressData = true;
    notifyListeners();
    final data = await api.smsCheck(phone, _errorStream);
    progressData = false;
    notifyListeners();
    if (data != null) {
      _smsCheckPhoneStream.sink.add(data);
    }
  }

  void regestry(String phone, String name, String sms) async {
    progressData = true;
    notifyListeners();
    final data = await api.regestry(phone, name, sms, _errorStream);
    progressData = false;
    notifyListeners();
    if (data != null) {
      PrefUtils.setToken(data.id);
      PrefUtils.setUser(data);
      _loginResponseStream.sink.add(data);
    }
  }

}
