import 'dart:io';

import 'package:aztorg_flutter/screen/splash/splash_viewmodel.dart';
import 'package:aztorg_flutter/utils/app_colors.dart';
import 'package:aztorg_flutter/utils/pref_utils.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:in_app_update/in_app_update.dart';
import 'package:stacked/stacked.dart';

import '../main/main_screen.dart';
import '../main/updatelanguage/update_language_screen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  var firstTimeRun = true;

  AppUpdateInfo? _updateState;

  @override
  void didChangeDependencies() {
    if (firstTimeRun) {
      updateLang();
      checkForUpdate();
      firstTimeRun = false;
    }
    super.didChangeDependencies();
  }

  void checkForUpdate() {
    InAppUpdate.checkForUpdate().then((state) async {
      print("JW update available");
      if (state.updateAvailability == UpdateAvailability.updateAvailable) {
        InAppUpdate.performImmediateUpdate().catchError((e) => print(e));
      }
    }).catchError((e) {
      print(e.toString());
    });
  }

  void updateLang() async {
    context.locale = (Locale(PrefUtils.getLang()));
  }

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<SplashViewModel>.reactive(
      builder: (context, viewModel, _) {
        return Container(
          color: AppColors.RED_COLOR,
          child: Stack(
            children: [
              Center(
                child: Image.asset(
                  "assets/images/logo_fill.png",
                  width: double.infinity,
                  height: 400,
                ),
              ),
              Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: const EdgeInsets.all(16),
                    child: CircularProgressIndicator(),
                  )),
            ],
          ),
        );
      },
      viewModelBuilder: () {
        return SplashViewModel();
      },
      onModelReady: (viewModel) {
        viewModel.errorData.listen((event) {
          showDialog(
              context: context,
              builder: (_) => AlertDialog(
                    title: Text("Error!"),
                    content: Text(event),
                    actions: [
                      TextButton(
                          onPressed: () {
                            if (Platform.isAndroid) {
                              SystemNavigator.pop();
                            } else {
                              exit(0);
                            }
                          },
                          child: Text(
                            "Exit",
                            style: TextStyle(color: Colors.black),
                          )),
                      TextButton(
                          onPressed: () {
                            viewModel.loadConfig();
                            Navigator.pop(context);

                          },
                          child: Text("Retry",
                              style: TextStyle(color: Colors.black))),
                    ],
                  ));
        });
        viewModel.bdmItemsData.listen((event) async {
          // await PrefUtils.setLangSelected(false);
          if (!PrefUtils.isLangSelected()) {
            await showModalBottomSheet(
                context: context, builder: (_) => UpdateLanguageScreen());
            // await Future.delayed(Duration(seconds: 2));
            await PrefUtils.setLangSelected(true);
            await context.setLocale(Locale(PrefUtils.getLang()));
            Get.updateLocale(Locale(PrefUtils.getLang()));
            print(PrefUtils.getLang());
          }
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (_) => MainScreen()),
              (route) => false);
        });
        viewModel.loadConfig();
      },
    );
  }
}
