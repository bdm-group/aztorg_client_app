import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

class HTMLViewScreen extends StatefulWidget {
  final String title;
  final String data;

  const HTMLViewScreen(this.title, this.data, {Key? key}) : super(key: key);

  @override
  State<HTMLViewScreen> createState() => _HTMLViewScreenState();
}

class _HTMLViewScreenState extends State<HTMLViewScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        child: Html(
          data: widget.data,
        ),
      ),
    );
  }
}
