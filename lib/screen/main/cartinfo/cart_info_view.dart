import 'package:aztorg_flutter/extensions/extensions.dart';
import 'package:aztorg_flutter/model/event_model.dart';
import 'package:event_bus/event_bus.dart';
import 'package:flutter/material.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/pref_utils.dart';
import '../main_screen.dart';
import 'package:easy_localization/easy_localization.dart';

class CartInfoView extends StatefulWidget {
  const CartInfoView({Key? key}) : super(key: key);

  @override
  _CartInfoViewState createState() => _CartInfoViewState();
}

class _CartInfoViewState extends State<CartInfoView> {
  var cartAmount = 0.0;
  var firstTimeRun = true;
  EventBus eventBus = EventBus();

  @override
  void didChangeDependencies() {
    if (firstTimeRun) {
      eventBus.on<EventModel>().listen((event) {
        calculateAmount();
      });

      calculateAmount();

      firstTimeRun = false;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return cartAmount == 0
        ? SizedBox()
        : ClipRRect(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(16), topRight: Radius.circular(16)),
            child: InkWell(
              onTap: () {
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                        builder: (_) => MainScreen(
                              selectedIndex: 2,
                            )),
                    (route) => false);
              },
              child: Container(
                color: AppColors.YELLOW_COLOR,
                padding:
                    EdgeInsets.only(top: 8, bottom: 8, left: 16, right: 16),
                child: Row(
                  children: [
                    Expanded(
                      flex: 3,
                      child: Text(
                        "cart".tr(),
                        textAlign: TextAlign.right,
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 16),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Text(
                        cartAmount.formattedAmountString(),
                        textAlign: TextAlign.right,
                        style: TextStyle(color: Colors.white, fontSize: 16),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
  }

  void calculateAmount() {
    setState(() {
      for (var it in PrefUtils.getCartList()) {
        cartAmount += it.count * it.price;
      }
    });
  }

  @override
  void dispose() {
    eventBus.destroy();
    super.dispose();
  }
}
