import 'dart:async';


import 'package:stacked/stacked.dart';

import '../../../api/api_service.dart';

class MakeRatingViewModel extends BaseViewModel {
  final api = ApiService();

  StreamController<String> _errorStream = StreamController();
  Stream<String> get errorData {
    return _errorStream.stream;
  }

  StreamController<int> _postRatingStream = StreamController();
  Stream<int> get postRatingData {
    return _postRatingStream.stream;
  }


  var progressData = false;

  void postRating(double rating, String description, String id) async {
    progressData = true;
    notifyListeners();
    final data = await api.postRating(rating, description, id, _errorStream);
    progressData = false;
    notifyListeners();
    if (data != null) {
      _postRatingStream.sink.add(data);
    }
  }

  @override
  void dispose() {
    _errorStream.close();
    super.dispose();
  }
}
