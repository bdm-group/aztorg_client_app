import 'dart:io';

import 'package:aztorg_flutter/extensions/extensions.dart';
import 'package:aztorg_flutter/model/product_model.dart';
import 'package:aztorg_flutter/utils/app_colors.dart';
import 'package:aztorg_flutter/view/custom_views.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:stacked/stacked.dart';

import 'makerating_viewmodel.dart';
import 'package:easy_localization/easy_localization.dart';

class MakeRatingScreen extends StatefulWidget {
  ProductModel item;

  MakeRatingScreen(this.item, {Key? key}) : super(key: key);

  @override
  _MakeRatingScreenState createState() => _MakeRatingScreenState();
}

class _MakeRatingScreenState extends State<MakeRatingScreen> {
  var rating = 5.0;
  var imagePath1 = "";
  var imagePath2 = "";
  var imagePath3 = "";
  var commentController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<MakeRatingViewModel>.reactive(
      builder: (context,  viewModel, child) {
        return Scaffold(
          backgroundColor: AppColors.BACKGROUND_COLOR,
          appBar: AppBar(
            title: Text("white_review".tr()),
          ),
          body: SingleChildScrollView(
            child: Container(
              padding: EdgeInsets.all(8),
              child: Column(
                children: [
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          Card(
                              child: CustomViews.buildNetworkImage(
                                  widget.item.images.length > 0
                                      ? widget.item.images.first
                                      : "",
                                  width: 80,
                                  height: 80)),
                          SizedBox(
                            width: 8,
                          ),
                          Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Text(
                                    widget.item.name,
                                    maxLines: 2,
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ),
                                  Text(
                                    widget.item.price.formattedAmountString(),
                                    maxLines: 1,
                                    style: TextStyle(fontSize: 16, color: Colors.black),
                                  ),
                                ],
                              ))
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Text(
                    "rate_product_".tr(),
                    style: TextStyle(fontSize: 16, color: Colors.black),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  RatingBar.builder(
                    initialRating: rating,
                    minRating: 1,
                    itemCount: 5,
                    itemSize: 50,
                    unratedColor: AppColors.GREY_LIGHT_COLOR,
                    itemBuilder: (context, _) => const Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                    onRatingUpdate: (rating) {
                      this.rating = rating;
                    },
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: CustomViews.buildMoreTextField(
                        "what_think_this_product".tr(), "write_your_opinion_".tr(), controller: commentController),
                  ),

                  SizedBox(
                    height: 16,
                  ),
                  viewModel.progressData ? Center(
                    child: CircularProgressIndicator(),
                  ) :Container(
                      width: double.infinity,
                      padding: EdgeInsets.only(top: 16, bottom: 16),
                      child: ElevatedButton(
                          onPressed: () {
                            viewModel.postRating(rating, commentController.text, widget.item.id);
                          }, child: Text("send".tr())))
                ],
              ),
            ),
          ),
        );
      },
      viewModelBuilder: () {
        return MakeRatingViewModel();
      },
      onModelReady: (viewModel){
        viewModel.errorData.listen((event) {
          ScaffoldMessenger.maybeOf(context)
              ?.showSnackBar(SnackBar(content: Text(event)));
        });
        viewModel.postRatingData.listen((event) {
          ScaffoldMessenger.maybeOf(context)
              ?.showSnackBar(SnackBar(content: Text("your_review_received".tr())));
          Navigator.pop(context);
        });
      },

    );
  }

  void showImageDialog(int index) {
    final ImagePicker _picker = ImagePicker();
    final alert = AlertDialog(
      backgroundColor: AppColors.BACKGROUND_COLOR,
      title: Text("select_source".tr(), style: TextStyle(color: Colors.black)),
      content: Text(""),
      actions: [
        TextButton(
            onPressed: () async {
              Navigator.pop(context);
              final image = await _picker.pickImage(
                  source: ImageSource.camera,
                  imageQuality: 60,
                  maxHeight: 1000,
                  maxWidth: 1000);
              if (image != null) {
                File? croppedFile = await ImageCropper().cropImage(
                  sourcePath: image.path,
                  compressQuality: 60,
                  aspectRatioPresets: [
                    CropAspectRatioPreset.square,
                    CropAspectRatioPreset.ratio3x2,
                    CropAspectRatioPreset.ratio5x4,
                    CropAspectRatioPreset.ratio16x9,
                  ],
                  androidUiSettings: AndroidUiSettings(
                      toolbarTitle: 'Редактирование изображений',
                      toolbarColor: Colors.black,
                      toolbarWidgetColor: Colors.white,
                      initAspectRatio: CropAspectRatioPreset.original,
                      lockAspectRatio: true),
                );
                if (croppedFile != null) {
                  setState(() {
                    setImagePath(index, croppedFile.path);
                  });
                }
              }
            },
            child: Text(
              "Камера",
              style: TextStyle(color: Colors.black),
            )),
        TextButton(
            onPressed: () async {
              Navigator.pop(context);
              final image = await _picker.pickImage(
                  source: ImageSource.gallery,
                  imageQuality: 60,
                  maxHeight: 1000,
                  maxWidth: 1000);
              if (image != null) {
                File? croppedFile = await ImageCropper().cropImage(
                  sourcePath: image.path,
                  compressQuality: 60,
                  aspectRatioPresets: [
                    CropAspectRatioPreset.square,
                    CropAspectRatioPreset.ratio3x2,
                    CropAspectRatioPreset.ratio5x4,
                    CropAspectRatioPreset.ratio16x9,
                  ],
                  androidUiSettings: AndroidUiSettings(
                      toolbarTitle: 'Редактирование изображений',
                      toolbarColor: Colors.red,
                      toolbarWidgetColor: Colors.white,
                      initAspectRatio: CropAspectRatioPreset.original,
                      lockAspectRatio: true),
                );
                if (croppedFile != null) {
                  setState(() {
                    setImagePath(index, croppedFile.path);
                  });
                }
              }
            },
            child: Text(
              "Галерея",
              style: TextStyle(color: Colors.black),
            )),
      ],
    );
    showDialog(context: context, builder: (_) => alert);
  }

  void setImagePath(int index, String path) {
    switch (index) {
      case 1:
        imagePath1 = path;
        break;
      case 2:
        imagePath2 = path;
        break;
      case 3:
        imagePath3 = path;
        break;
    }
  }

  String getImagePath(int index) {
    switch (index) {
      case 1:
        return imagePath1;
      case 2:
        return imagePath2;
      case 3:
        return imagePath3;
    }
    return "";
  }
}
