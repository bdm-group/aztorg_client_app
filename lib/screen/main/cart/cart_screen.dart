import 'package:aztorg_flutter/extensions/extensions.dart';
import 'package:aztorg_flutter/model/make_order_model.dart';
import 'package:aztorg_flutter/model/make_order_product.dart';
import 'package:aztorg_flutter/model/response/login_response.dart';
import 'package:aztorg_flutter/screen/auth/auth_screen.dart';
import 'package:aztorg_flutter/screen/main/cart/cart_viewmodel.dart';
import 'package:aztorg_flutter/screen/main/makeorder/make_order_screen.dart';
import 'package:aztorg_flutter/view/cart_item_layout.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/pref_utils.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({Key? key}) : super(key: key);

  @override
  _CartScreenState createState() {
    return _CartScreenState();
  }
}

class _CartScreenState extends State<CartScreen> {
  double totalSum = 0.0;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    updateTotalAmount();
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<CartViewModel>.reactive(
      viewModelBuilder: () {
        return CartViewModel();
      },
      builder: (context, viewModel, child) {
        return Stack(
          children: [
            Container(
                color: AppColors.BACKGROUND_COLOR,
                child: Column(
                  children: [
                    Expanded(
                      child: ListView.builder(
                          //
                          itemCount: viewModel.cartProductList.length,
                          shrinkWrap: true,
                          itemBuilder: (_, position) => CartItemLayout(
                                  viewModel.cartProductList[position], () {
                                updateTotalAmount();
                                if (PrefUtils.getCartList().isEmpty) {
                                  setState(() {
                                    viewModel.cartProductList = [];
                                  });
                                }
                              }, () {
                                setState(() {
                                  viewModel.cartProductList.remove(
                                      viewModel.cartProductList[position]);
                                  updateTotalAmount();
                                });
                                // if (PrefUtils.getCartList().isNotEmpty) {
                                //   viewModel.getProductList();
                                // } else {
                                //   setState(() {
                                //     viewModel.cartProductList = [];
                                //   });
                                // }
                              })),
                    ),
                    if (!viewModel.progressData &&
                        PrefUtils.getCartList().isNotEmpty)
                      Container(
                        padding: const EdgeInsets.all(16),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Colors.white,
                            border: Border.all(
                                color: Colors.grey.shade200, width: 1)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Row(
                              children: [
                                Expanded(
                                    child: Text(
                                  "Количество (" +
                                      viewModel.cartProductList.length
                                          .toString() +
                                      ")",
                                  style: TextStyle(
                                      color: AppColors.GREY_TEXT_COLOR,
                                      fontSize: 16),
                                )),
                                Text(
                                  "",
                                  style: TextStyle(
                                      color: AppColors.BLACK_COLOR,
                                      fontSize: 16),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 8,
                            ),
                            DottedLine(
                              dashColor: Colors.grey.shade500,
                            ),
                            const SizedBox(
                              height: 8,
                            ),
                            Row(
                              children: [
                                Expanded(
                                    child: Text(
                                  "Общая сумма",
                                  style: TextStyle(
                                      color: AppColors.BLACK_COLOR,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                )),
                                Text(
                                  totalSum.formattedAmountString(),
                                  style: TextStyle(
                                      color: AppColors.BLACK_COLOR,
                                      fontSize: 16),
                                ),
                              ],
                            ),
                            const SizedBox(
                              height: 16,
                            ),
                            ElevatedButton(
                                onPressed: () {
                                  PrefUtils.getToken().isNotEmpty
                                      ? PrefUtils.getCartList().isEmpty
                                          ? ScaffoldMessenger.maybeOf(context)
                                              ?.showSnackBar(const SnackBar(
                                                  content:
                                                      Text("Корзина пуста!")))
                                          : Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (_) => MakeOrderScreen(MakeOrderModel(
                                                      PrefUtils.getSelectedBranch()!
                                                          .id,
                                                      "",
                                                      viewModel.cartProductList
                                                          .map((item) =>
                                                              MakeOrderProduct(
                                                                  PrefUtils.getCartCount(
                                                                      item.id),
                                                                  item.price,
                                                                  item.store_id,
                                                                  item.barcode,
                                                                  item.id,
                                                                  item.price *
                                                                      PrefUtils.getCartCount(item.id)))
                                                          .toList(),
                                                    true,
                                                    "",
                                                    "",
                                                    0.0
                                                  ))))
                                      : Navigator.push(context, MaterialPageRoute(builder: (_) => AuthScreen()));
                                },
                                child: Text("Заказ")),
                          ],
                        ),
                      )
                  ],
                )),
            viewModel.progressData
                ? const Center(child: CircularProgressIndicator())
                : const Center(),
            if (PrefUtils.getCartList().isEmpty)
              Center(
                child: Text(
                  "Корзина пуста!",
                  style: TextStyle(
                    color: Colors.grey,
                    fontSize: 16,
                  ),
                ),
              )
          ],
        );
      },
      onModelReady: (viewModel) {
        viewModel.errorData.listen((event) {
          ScaffoldMessenger.maybeOf(context)
              ?.showSnackBar(SnackBar(content: Text(event)));
        });
        if (PrefUtils.getCartList().isNotEmpty) viewModel.getProductList();

        viewModel.makeOrderData.listen((event) {
          PrefUtils.clearCart();
          ScaffoldMessenger.maybeOf(context)
              ?.showSnackBar(SnackBar(content: Text("!")));
        });
      },
    );
  }

  void updateTotalAmount() {
    totalSum = 0.0;
    for (var it in PrefUtils.getCartList()) {
      totalSum += it.count * it.price;
    }
    setState(() {
      totalSum;
    });
  }
}
