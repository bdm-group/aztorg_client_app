import 'dart:async';

import 'package:aztorg_flutter/model/make_order_model.dart';
import 'package:aztorg_flutter/model/product_model.dart';
import 'package:aztorg_flutter/model/request/product_request_model.dart';
import 'package:aztorg_flutter/utils/pref_utils.dart';
import 'package:stacked/stacked.dart';

import '../../../api/api_service.dart';

class MakeOrderViewModel extends BaseViewModel {
  final api = ApiService();

  StreamController<String> _errorStream = StreamController();

  Stream<String> get errorData {
    return _errorStream.stream;
  }

  final StreamController<int> _makeOrderData = StreamController();
  Stream<int> get makeOrderData {
    return _makeOrderData.stream;
  }

  var progressData = false;

  void makeOrder(MakeOrderModel orderModel) async {
    progressData = true;
    notifyListeners();
    final data = await api.makeOrder(orderModel, _errorStream);
    progressData = false;
    notifyListeners();
    if (data != null) {
      _makeOrderData.sink.add(data);
    }
  }

  @override
  void dispose() {
    _errorStream.close();
    super.dispose();
  }
}
