import 'dart:async';
import 'dart:ui';

import 'package:aztorg_flutter/extensions/extensions.dart';
import 'package:aztorg_flutter/model/make_order_model.dart';
import 'package:aztorg_flutter/screen/main/makeorder/successorder/success_order_screen.dart';
import 'package:aztorg_flutter/utils/pref_utils.dart';
import 'package:aztorg_flutter/view/custom_views.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:stacked/stacked.dart';

import '../../../utils/app_colors.dart';
import 'makeorder_viewmodel.dart';

class MakeOrderScreen extends StatefulWidget {
  MakeOrderModel orderModel;

  MakeOrderScreen(this.orderModel, {Key? key}) : super(key: key);

  @override
  _MakeOrderScreenState createState() => _MakeOrderScreenState();
}

class _MakeOrderScreenState extends State<MakeOrderScreen> {
  var commentInputController = TextEditingController();
  var isDelivery = true;
  var isCashback = false;
  List<Marker> markers = [];
  LatLng? selectedPosition;
  LatLng? currentPosition;
  Completer<GoogleMapController> _controller = Completer();

  @override
  void initState() {
    getLocation();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var summa = 0.0;
    widget.orderModel.products.forEach((element) {
      summa += element.summa;
    });
    return ViewModelBuilder<MakeOrderViewModel>.reactive(
      viewModelBuilder: () {
        return MakeOrderViewModel();
      },
      builder: (context, viewModel, child) {
        return Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              title: Text("make_order".tr()),
            ),
            body: Container(
                child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 16, right: 16, top: 16),
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            "${"quantity".tr()} (" +
                                widget.orderModel.products.length.toString() +
                                ")",
                            style: TextStyle(
                                color: AppColors.BLACK_COLOR, fontSize: 16),
                          ),
                        ),
                        Text(
                          summa.formattedAmountString(),
                          style: TextStyle(
                              color: AppColors.BLACK_COLOR,
                              fontWeight: FontWeight.bold,
                              fontSize: 20),
                        ),
                      ],
                    ),
                  ),
                  if(isCashback)Padding(
                    padding: const EdgeInsets.only(right: 16),
                    child: Text(
                      "-${summa < PrefUtils.getUser().keshback ? summa.formattedAmountString() :PrefUtils.getUser().keshback.formattedAmountString()}",
                      textAlign: TextAlign.end,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,),
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 16, right: 16, bottom: 16),
                    child: Row(
                      children: [
                        Expanded(
                            child: Text(
                          "cashback".tr(),
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.bold),
                        )),
                        Text(
                          PrefUtils.getUser().keshback.formattedAmountString(),
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                          ),
                        ),
                        Checkbox(value: isCashback, onChanged: (_) {
                          setState(() {
                            isCashback = !isCashback;
                          });
                        }),
                      ],
                    ),
                  ),
                  Container(
                    height: 80,
                    color: Colors.black,
                    child: Stack(
                      children: [
                        Align(
                          child: Image.asset("assets/images/ripple_layer.png"),
                          alignment: Alignment.centerRight,
                        ),
                        Center(
                          child: Padding(
                            padding: const EdgeInsets.all(16),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Text(
                                    PrefUtils.getUser().name,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20),
                                  ),
                                ),
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(24),
                                  child: Container(
                                    color: Colors.grey,
                                    height: 48,
                                    width: 48,
                                    child: CustomViews
                                        .buildNetworkImageWithoutRadius(
                                            PrefUtils.getUser().image),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 16, bottom: 8),
                    child: Text(
                      "delivery_type_".tr(),
                      style: TextStyle(color: Colors.black, fontSize: 14),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 16, right: 16),
                    child: Container(
                      width: double.infinity,
                      height: 40,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.grey.shade300),
                      child: Padding(
                        padding: const EdgeInsets.all(4),
                        child: Row(
                          children: [
                            Expanded(
                              flex: 1,
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    isDelivery = false;
                                  });
                                },
                                child: Container(
                                  height: double.infinity,
                                  decoration: isDelivery
                                      ? null
                                      : BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(16),
                                          color: AppColors.BLACK_COLOR,
                                        ),
                                  child: Center(
                                    child: Text(
                                      "by_yourself".tr(),
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: !isDelivery
                                              ? Colors.white
                                              : Colors.black,
                                          fontSize: 14),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: InkWell(
                                onTap: () {
                                  setState(() {
                                    isDelivery = true;
                                  });
                                },
                                child: Container(
                                  height: double.infinity,
                                  decoration: !isDelivery
                                      ? null
                                      : BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(16),
                                          color: AppColors.BLACK_COLOR,
                                        ),
                                  child: Center(
                                    child: Text(
                                      "delivery".tr(),
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: isDelivery
                                              ? Colors.white
                                              : Colors.black,
                                          fontSize: 14),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        if (isDelivery)
                          Text(
                            "address_order_".tr(),
                            style: TextStyle(color: Colors.black, fontSize: 14),
                          ),
                        if (isDelivery)
                          SizedBox(
                            height: 8,
                          ),
                        if (isDelivery)
                          Card(
                            child: Container(
                              height: 230,
                              child: GoogleMap(
                                gestureRecognizers:
                                    <Factory<OneSequenceGestureRecognizer>>[
                                  new Factory<OneSequenceGestureRecognizer>(
                                    () => new EagerGestureRecognizer(),
                                  ),
                                ].toSet(),
                                myLocationEnabled: true,
                                myLocationButtonEnabled: true,
                                zoomControlsEnabled: true,
                                markers: Set.of(markers),
                                onMapCreated: (controller) {
                                  _controller.complete(controller);
                                },
                                onTap: (position) async {
                                  setState(() {
                                    selectedPosition = position;
                                    markers.clear();
                                    markers.add(Marker(
                                        markerId: MarkerId("1"),
                                        position: position));
                                  });
                                },
                                initialCameraPosition: CameraPosition(
                                    target: selectedPosition != null
                                        ? LatLng(selectedPosition!.latitude,
                                            selectedPosition!.longitude)
                                        : currentPosition != null
                                            ? LatLng(currentPosition!.latitude,
                                                currentPosition!.longitude)
                                            : LatLng(40.3656421, 71.7834312),
                                    zoom: 12),
                              ),
                            ),
                          ),
                        if (isDelivery)
                          Text(
                            "your_can_select_location_with_touch".tr(),
                            style: TextStyle(
                                fontSize: 12, color: AppColors.GREY_TEXT_COLOR),
                          ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 16, right: 16),
                    child: CustomViews.buildMoreTextField(
                        "comment_for_order".tr(),
                        "input_comment_for_order".tr(),
                        controller: commentInputController),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16),
                    child: viewModel.progressData
                        ? Center(
                            child: CircularProgressIndicator(),
                          )
                        : ElevatedButton(
                            onPressed: () {
                              if (isDelivery && selectedPosition == null) {
                                ScaffoldMessenger.maybeOf(context)
                                    ?.showSnackBar(SnackBar(
                                        content: Text(
                                            "please_select_address".tr())));
                                return;
                              }
                              widget.orderModel.soboy = !isDelivery;
                              widget.orderModel.lat =
                                  selectedPosition?.latitude.toString() ?? "";
                              widget.orderModel.long =
                                  selectedPosition?.longitude.toString() ?? "";
                              widget.orderModel.comment =
                                  commentInputController.text;
                              var cashbackAmount = 0.0;
                              if(summa < PrefUtils.getUser().keshback){
                                cashbackAmount = summa;
                              }else{
                                cashbackAmount = PrefUtils.getUser().keshback;
                              }
                              widget.orderModel.keshback = isCashback ? cashbackAmount : 0.0;

                              viewModel.makeOrder(widget.orderModel);
                            },
                            child: Text("make_order".tr())),
                  ),
                ],
              ),
            )));
      },
      onModelReady: (viewModel) {
        viewModel.errorData.listen((event) {
          ScaffoldMessenger.maybeOf(context)
              ?.showSnackBar(SnackBar(content: Text(event)));
        });
        viewModel.makeOrderData.listen((event) {
          PrefUtils.clearCart();
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (_) => SuccessOrderScreen(event)),
              (route) => false);
        });
      },
    );
  }

  void getLocation() async {
    try {
      await Geolocator.requestPermission();
      final location = await Geolocator.getCurrentPosition();
      setState(() {
        currentPosition = LatLng(location.latitude, location.longitude);
      });
    } catch (e) {
      print(e.toString());
    }
  }
}
