import 'dart:async';

import 'package:aztorg_flutter/model/product_model.dart';
import 'package:aztorg_flutter/model/rating_data_model.dart';
import 'package:aztorg_flutter/model/request/product_request_model.dart';
import 'package:aztorg_flutter/utils/pref_utils.dart';
import 'package:stacked/stacked.dart';

import '../../../api/api_service.dart';

class ProductDetailViewModel extends BaseViewModel {
  final ProductModel item;

  ProductDetailViewModel(this.item);

  final api = ApiService();

  StreamController<String> _errorStream = StreamController();

  Stream<String> get errorData {
    return _errorStream.stream;
  }


  var progressData = false;

  List<ProductModel> productList = [];
  RatingDataModel? ratingDataModel;

  void getSimilarProductList() async {
    productList = await api.getProductList(
        ProductRequestModel(PrefUtils.getSelectedBranch()?.id ?? "", "",
            item.category_id, "", false, []),
        _errorStream);
    productList.removeWhere((element) => element.id == item.id);
    notifyListeners();
  }

  void getRatingList() async {
    ratingDataModel = await api.getRatingData(item.id, _errorStream);
    notifyListeners();
  }

  @override
  void dispose() {
    _errorStream.close();
    super.dispose();
  }
}
