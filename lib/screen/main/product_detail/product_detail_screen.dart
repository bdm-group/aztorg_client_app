import 'dart:async';

import 'package:aztorg_flutter/extensions/extensions.dart';
import 'package:aztorg_flutter/model/basket_model.dart';
import 'package:aztorg_flutter/model/product_model.dart';
import 'package:aztorg_flutter/screen/main/product_detail/product_detail_viewmodel.dart';
import 'package:aztorg_flutter/screen/main/ratings/ratings_screen.dart';
import 'package:aztorg_flutter/view/custom_views.dart';
import 'package:aztorg_flutter/view/product_item_view.dart';
import 'package:badges/badges.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:stacked/stacked.dart';

import '../../../utils/app_colors.dart';
import '../../../utils/pref_utils.dart';
import '../main_screen.dart';
import '../makerating/make_rating_screen.dart';

class ProductDetailScreen extends StatefulWidget {
  final ProductModel item;

  const ProductDetailScreen(this.item, {Key? key}) : super(key: key);

  @override
  _ProductDetailScreenState createState() => _ProductDetailScreenState();
}

class _ProductDetailScreenState extends State<ProductDetailScreen> {
  final imagePageController = PageController();
  var currentImagePage = 0;
  Timer? timer;
  var cartCount = 0.0;

  @override
  void didChangeDependencies() {
    cartCount = 0.0;
    try {
      if (PrefUtils.getCartList().isNotEmpty) {
        cartCount = PrefUtils.getCartList()
            .firstWhere((element) => element.id == widget.item.id)
            .count;
      } else {
        cartCount = 0;
      }
    } on Exception catch (exception) {
      cartCount = 0;
      debugPrint("exception_ass_---> " + exception.toString());
    } catch (error) {
      debugPrint("error_ass_---> " + error.toString());
    }

    debugPrint("cartCount_ass_--> $cartCount");
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(const Duration(seconds: 4), (timer) {
      imagePageController
          .jumpToPage(currentImagePage % widget.item.images.length);
      currentImagePage++;
    });

    // cartCount = PrefUtils.getCartList()?.firstWhere((element) => element.id == widget.item.id) as int;
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<ProductDetailViewModel>.reactive(
      builder: (context, viewModel, child) {
        return Scaffold(
          appBar: AppBar(
            title: Text(widget.item.name),
            actions: [
              if (PrefUtils.getCartList().length > 0)
                InkWell(
                  onTap: () {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                            builder: (_) => MainScreen(
                                  selectedIndex: 2,
                                )),
                        (route) => false);
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Badge(
                      animationType: BadgeAnimationType.scale,
                      badgeContent: Text(
                        PrefUtils.getCartList().length.toString(),
                        style: TextStyle(color: Colors.white),
                      ),
                      child: Icon(Icons.shopping_cart_outlined),
                    ),
                  ),
                ),
              SizedBox(
                width: 16,
              )
            ],
          ),
          backgroundColor: Colors.white,
          body: Container(
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Container(
                          height: 240,
                          child: PageView(
                            controller: imagePageController,
                            children: widget.item.images
                                .map((image) => CustomViews.buildNetworkImage(
                                      image,
                                      fit: BoxFit.contain,
                                      width: double.infinity,
                                      height: double.infinity,
                                    ))
                                .toList(),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(16.0),
                          child: Align(
                              alignment: Alignment.center,
                              child: SmoothPageIndicator(
                                controller: imagePageController,
                                count: widget.item.images.length,
                                effect: SlideEffect(
                                    spacing: 8.0,
                                    radius: 4.0,
                                    dotWidth: 8.0,
                                    dotHeight: 8.0,
                                    paintStyle: PaintingStyle.fill,
                                    dotColor: AppColors.LIGHT_RED_COLOR,
                                    activeDotColor: AppColors.RED_COLOR),
                              )),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 16, right: 16),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                      child: Text(
                                    widget.item.name,
                                    style: TextStyle(
                                        fontSize: 20,
                                        color: AppColors.BLACK_COLOR,
                                        fontWeight: FontWeight.bold),
                                  )),
                                  IconButton(
                                      onPressed: () {
                                        PrefUtils.setFavoriteProduct(
                                            widget.item);
                                        setState(() {
                                          widget.item.favorite =
                                              !widget.item.favorite;
                                        });
                                      },
                                      iconSize: 24,
                                      icon: Icon(
                                        widget.item.favorite
                                            ? Icons.favorite_rounded
                                            : Icons.favorite_border,
                                        color: widget.item.favorite
                                            ? AppColors.RED_COLOR
                                            : AppColors.GREY_COLOR,
                                      )),
                                ],
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              Row(
                                children: [
                                  Expanded(
                                    child: InkWell(
                                      child: RatingBar.builder(
                                        ignoreGestures: true,
                                        initialRating: widget.item.rating,
                                        minRating: 1,
                                        itemCount: 5,
                                        itemSize: 20,
                                        unratedColor:
                                            AppColors.GREY_LIGHT_COLOR,
                                        itemBuilder: (context, _) => const Icon(
                                          Icons.star,
                                          color: Colors.amber,
                                        ),
                                        onRatingUpdate: (rating) {},
                                      ),
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                              builder: (context) =>
                                                  MakeRatingScreen(widget.item),
                                            ));
                                      },
                                    ),
                                  ),
                                  Text(
                                    "product_code_".tr() + widget.item.barcode,
                                    style: TextStyle(
                                        fontSize: 12,
                                        color: AppColors.GREY_TEXT_COLOR),
                                  )
                                ],
                              ),
                              const SizedBox(
                                height: 16,
                              ),
                              Row(
                                children: [
                                  Text(
                                    widget.item.price.formattedAmountString(),
                                    style: TextStyle(
                                        fontSize: 20,
                                        color: AppColors.BLACK_COLOR,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  const SizedBox(
                                    width: 8,
                                  ),
                                  if (widget.item.old_price > 0)
                                    Text(
                                      widget.item.old_price.toString(),
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold,
                                          decorationColor:
                                              AppColors.GREY_TEXT_COLOR,
                                          decorationStyle:
                                              TextDecorationStyle.solid,
                                          decoration:
                                              TextDecoration.lineThrough,
                                          color: AppColors.GREY_TEXT_COLOR),
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                ],
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              Divider(
                                color: AppColors.GREY_LIGHT_COLOR,
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              Text(
                                "characteristic".tr(),
                                style: TextStyle(
                                    fontSize: 16,
                                    color: AppColors.BLACK_COLOR,
                                    fontWeight: FontWeight.bold),
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              Text(
                                widget.item.description,
                                style: TextStyle(
                                    fontSize: 16,
                                    color: AppColors.GREY_TEXT_COLOR),
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              if (viewModel.ratingDataModel != null &&
                                  viewModel.ratingDataModel!.reviews.isNotEmpty)
                                Column(
                                  crossAxisAlignment:
                                      CrossAxisAlignment.stretch,
                                  children: [
                                    Divider(
                                      color: AppColors.GREY_LIGHT_COLOR,
                                    ),
                                    const SizedBox(
                                      height: 8,
                                    ),
                                    Row(
                                      children: [
                                        Expanded(
                                            child: Text(
                                          "product_reviews".tr(),
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold,
                                              color: AppColors.BLACK_COLOR),
                                        )),
                                        TextButton(
                                          onPressed: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (_) =>
                                                        RatingsScreen(viewModel
                                                            .ratingDataModel!)));
                                          },
                                          child: Text(
                                            "learn_more".tr(),
                                            style: TextStyle(
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold,
                                                color: AppColors.RED_COLOR),
                                          ),
                                        ),
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 8,
                                    ),
                                    Row(
                                      children: [
                                        RatingBar.builder(
                                          ignoreGestures: true,
                                          initialRating: widget.item.rating,
                                          minRating: 1,
                                          itemCount: 5,
                                          itemSize: 20,
                                          unratedColor:
                                              AppColors.GREY_LIGHT_COLOR,
                                          itemBuilder: (context, _) =>
                                              const Icon(
                                            Icons.star,
                                            color: Colors.amber,
                                          ),
                                          onRatingUpdate: (rating) {
                                            print(rating);
                                          },
                                        ),
                                        const SizedBox(
                                          width: 8,
                                        ),
                                        Text(
                                          widget.item.rating.toStringAsFixed(1),
                                          style: TextStyle(
                                              fontSize: 12,
                                              color: AppColors.BLACK_COLOR,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        const SizedBox(
                                          width: 8,
                                        ),
                                        Text(
                                          "(${viewModel.ratingDataModel?.reviews.length ?? 0})"
                                              .toString(),
                                          style: TextStyle(
                                              fontSize: 12,
                                              color: AppColors.GREY_TEXT_COLOR),
                                        )
                                      ],
                                    ),
                                    const SizedBox(
                                      width: 16,
                                    ),
                                    ListTile(
                                      leading: ClipRRect(
                                        borderRadius: BorderRadius.circular(20),
                                        child: Container(
                                          color: Colors.grey,
                                          height: 40,
                                          width: 40,
                                          child: CustomViews.buildNetworkImage(
                                              viewModel.ratingDataModel?.reviews
                                                  .first.user_avatar),
                                        ),
                                      ),
                                      title: Text(
                                        viewModel.ratingDataModel?.reviews.first
                                                .user_fullaname ??
                                            "",
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            fontSize: 16,
                                            color: AppColors.BLACK_COLOR,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      subtitle: RatingBar.builder(
                                        initialRating: viewModel.ratingDataModel
                                                ?.reviews.first.rating ??
                                            0.0,
                                        minRating: 1,
                                        itemCount: 5,
                                        itemSize: 20,
                                        unratedColor:
                                            AppColors.GREY_LIGHT_COLOR,
                                        itemBuilder: (context, _) => const Icon(
                                          Icons.star,
                                          color: Colors.amber,
                                        ),
                                        onRatingUpdate: (rating) {
                                          print(rating);
                                        },
                                      ),
                                    ),
                                    Text(
                                      viewModel.ratingDataModel?.reviews.first
                                              .comment ??
                                          "",
                                      style: TextStyle(
                                          fontSize: 14,
                                          color: AppColors.GREY_TEXT_COLOR),
                                    ),
                                    const SizedBox(
                                      height: 8,
                                    ),
                                    Text(
                                      viewModel.ratingDataModel?.reviews.first
                                              .date_time.formattedDateTime ??
                                          "",
                                      style: TextStyle(
                                          fontSize: 12,
                                          color: AppColors.GREY_TEXT_COLOR),
                                    ),
                                    const SizedBox(
                                      height: 8,
                                    ),
                                  ],
                                ),
                              Divider(),
                              const SizedBox(
                                height: 8,
                              ),
                              OutlinedButton(
                                  style: ButtonStyle(
                                    shape: MaterialStateProperty.all(
                                      RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(30.0),
                                      ),
                                    ),
                                    side: MaterialStateProperty.all(
                                      BorderSide(
                                          width: 2,
                                          color: AppColors.GREY_LIGHT_COLOR),
                                    ),
                                  ),
                                  onPressed: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              MakeRatingScreen(widget.item),
                                        ));
                                  },
                                  child: Text(
                                    "review".tr(),
                                    style: TextStyle(color: Colors.black),
                                  )),
                              const SizedBox(
                                height: 8,
                              ),
                              Divider(),
                              const SizedBox(
                                height: 8,
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 16, right: 16),
                          child: Text(
                            "you_may_also_like".tr(),
                            style: TextStyle(
                                fontSize: 14,
                                fontWeight: FontWeight.bold,
                                color: AppColors.BLACK_COLOR),
                          ),
                        ),
                        const SizedBox(
                          width: 16,
                        ),
                        Container(
                          height: 300,
                          child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: viewModel.productList.length,
                              shrinkWrap: true,
                              itemBuilder: (_, position) {
                                return Container(
                                    width: 170,
                                    padding: EdgeInsets.all(8),
                                    child: ProductItemView(
                                        viewModel.productList[position], () {
                                      //updateFavorite
                                    }));
                              }),
                        )
                      ],
                    ),
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      height: 2,
                      color: Colors.grey.shade100,
                    ),
                    Container(
                      color: Colors.white,
                      padding: EdgeInsets.all(10),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Text(
                                  (widget.item.price * cartCount)
                                      .formattedAmountString(),
                                  style: const TextStyle(
                                      color: Colors.black,
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                          cartCount == 0
                              ? ElevatedButton(
                                  onPressed: () {
                                    setState(() {
                                      cartCount = 1;
                                    });
                                    addToCart(
                                        widget.item.id,
                                        cartCount,
                                        widget.item.price,
                                        widget.item.category_id);
                                  },
                                  child: Text("add_2_cart".tr()),
                                )
                              : Container(
                                  decoration: BoxDecoration(
                                      color: AppColors.GREY_LIGHT_COLOR,
                                      borderRadius: BorderRadius.circular(8),
                                      border: Border.all(
                                          color: AppColors.GREY_LIGHT_COLOR,
                                          width: 1)),
                                  child: Row(
                                    children: [
                                      InkWell(
                                        onTap: () {
                                          setState(() {
                                            cartCount--;
                                          });
                                          addToCart(
                                              widget.item.id,
                                              cartCount,
                                              widget.item.price,
                                              widget.item.category_id);
                                        },
                                        child: Container(
                                          padding: const EdgeInsets.only(
                                              left: 20,
                                              right: 20,
                                              top: 4,
                                              bottom: 4),
                                          decoration: const BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(8),
                                                bottomLeft: Radius.circular(8)),
                                          ),
                                          child: Center(
                                              child: Text(
                                            "-",
                                            style: TextStyle(
                                                color:
                                                    AppColors.GREY_TEXT_COLOR,
                                                fontSize: 24),
                                          )),
                                        ),
                                      ),
                                      Center(
                                          child: Padding(
                                        padding: const EdgeInsets.only(
                                            left: 16, right: 16),
                                        child: Row(
                                          children: [
                                            Text(
                                              "$cartCount",
                                              style: TextStyle(
                                                  color:
                                                      AppColors.GREY_TEXT_COLOR,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 20),
                                            ),
                                            const SizedBox(
                                              width: 4,
                                            ),
                                            Text(
                                              widget.item.unity,
                                              style: TextStyle(
                                                  color:
                                                      AppColors.GREY_TEXT_COLOR,
                                                  fontSize: 14),
                                            ),
                                          ],
                                        ),
                                      )),
                                      InkWell(
                                        onTap: () {
                                          setState(() {
                                            cartCount++;
                                            addToCart(
                                                widget.item.id,
                                                cartCount,
                                                widget.item.price,
                                                widget.item.category_id);
                                          });
                                        },
                                        child: Container(
                                          padding: const EdgeInsets.only(
                                              left: 20,
                                              right: 20,
                                              top: 4,
                                              bottom: 4),
                                          decoration: const BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.only(
                                                topRight: Radius.circular(8),
                                                bottomRight:
                                                    Radius.circular(8)),
                                          ),
                                          child: Center(
                                              child: Text(
                                            "+",
                                            style: TextStyle(
                                                color:
                                                    AppColors.GREY_TEXT_COLOR,
                                                fontSize: 24),
                                          )),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                        ],
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        );
      },
      viewModelBuilder: () => ProductDetailViewModel(widget.item),
      onModelReady: (viewModel) {
        viewModel.errorData.listen((event) {
          ScaffoldMessenger.maybeOf(context)
              ?.showSnackBar(SnackBar(content: Text(event)));
        });

        viewModel.getSimilarProductList();
        viewModel.getRatingList();
      },
    );
  }

  void addToCart(String id, double count, double price, String store_id) {
    PrefUtils.addToCart(BasketModel(id, count, price, store_id));
  }
}
