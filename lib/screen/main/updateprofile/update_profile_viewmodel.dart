import 'dart:async';


import 'package:stacked/stacked.dart';

import '../../../api/api_service.dart';

class UpdateProfileViewModel extends BaseViewModel {
  final api = ApiService();

  StreamController<String> _errorStream = StreamController();
  Stream<String> get errorData {
    return _errorStream.stream;
  }

  StreamController<bool> _updateAvatarStream = StreamController();
  Stream<bool> get updateAvatarData {
    return _updateAvatarStream.stream;
  }

  StreamController<bool> _updateProfileStream = StreamController();
  Stream<bool> get updateProfileData {
    return _updateProfileStream.stream;
  }


  var progressData = false;

  void updateAvatar(String image) async {
    progressData = true;
    notifyListeners();
    final data = await api.updateAvatar(image, _errorStream);
    progressData = false;
    notifyListeners();
    if (data != null) {
      _updateAvatarStream.sink.add(data);
    }
  }

  void updateProfile(String fullname, String phone, String lat, String long) async {
    progressData = true;
    notifyListeners();
    final data = await api.updateProfile(fullname, phone, lat, long, _errorStream);
    progressData = false;
    notifyListeners();
    if (data != null) {
      _updateProfileStream.sink.add(data);
    }
  }

  @override
  void dispose() {
    _errorStream.close();
    super.dispose();
  }
}
