import 'dart:convert';
import 'dart:io';

import 'package:aztorg_flutter/screen/main/updateprofile/update_profile_viewmodel.dart';
import 'package:aztorg_flutter/utils/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:stacked/stacked.dart';

import '../../../utils/pref_utils.dart';
import '../../../view/custom_views.dart';

class UpdateProfileScreen extends StatefulWidget {
  const UpdateProfileScreen({Key? key}) : super(key: key);

  @override
  _UpdateProfileScreenState createState() => _UpdateProfileScreenState();
}

class _UpdateProfileScreenState extends State<UpdateProfileScreen> {
  var imagePath1 = "";
  var _phoneController = TextEditingController(text: PrefUtils.getUser().phone);
  var _fullnameController =
      TextEditingController(text: PrefUtils.getUser().name);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<UpdateProfileViewModel>.reactive(
      viewModelBuilder: () {
        return UpdateProfileViewModel();
      },
      builder: (BuildContext context, viewModel, Widget? child) {
        return Container(
          height: 500,
          padding: EdgeInsets.only(
            bottom: MediaQuery.of(context).viewInsets.bottom,
          ),
          color: AppColors.BACKGROUND_COLOR,
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.only(
                left: 16,
                right: 16,
                top: 16,
                bottom: 8,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Center(
                    child: Column(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(50),
                          child: Container(
                            color: Colors.grey,
                            height: 100,
                            width: 100,
                            child: imagePath1.isNotEmpty ? Image.file(File(imagePath1)) : CustomViews.buildNetworkImageWithoutRadius(
                                PrefUtils.getUser().image),
                          ),
                        ),
                        viewModel.progressData
                            ? Center(
                                child: CircularProgressIndicator(),
                              )
                            : OutlinedButton(
                                style: ButtonStyle(
                                  shape: MaterialStateProperty.all(
                                    RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(30.0),
                                    ),
                                  ),
                                  side: MaterialStateProperty.all(
                                    BorderSide(
                                        width: 2, color: AppColors.RED_COLOR),
                                  ),
                                ),
                                onPressed: () {
                                  showImageDialog(viewModel);
                                },
                                child: Text(
                                  "Загрузить аватар",
                                  style: TextStyle(color: Colors.black),
                                )),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  CustomViews.buildTextField(
                      "Телефонный номер", "Телефонный номер",
                      controller: _phoneController, enabled: false),
                  SizedBox(
                    height: 16,
                  ),
                  CustomViews.buildTextField(
                    "Имя",
                    "",
                    controller: _fullnameController,
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  viewModel.progressData
                      ? Center(
                          child: CircularProgressIndicator(),
                        )
                      : ElevatedButton(
                          onPressed: () {
                            viewModel.updateProfile(_fullnameController.text,
                                _phoneController.text, "", "");
                          },
                          child: Text("Сохранять")),
                  SizedBox(
                    height: 8,
                  ),
                ],
              ),
            ),
          ),
        );
      },
      onModelReady: (viewModel) {
        viewModel.errorData.listen((event) {
          ScaffoldMessenger.maybeOf(context)
              ?.showSnackBar(SnackBar(content: Text(event)));
        });
        viewModel.updateAvatarData.listen((event) {
          if (event) {
            ScaffoldMessenger.maybeOf(context)?.showSnackBar(
                SnackBar(content: Text("Аватар профиля обновлен")));
          }
        });
        viewModel.updateProfileData.listen((event) {
          if (event) {
            ScaffoldMessenger.maybeOf(context)
                ?.showSnackBar(SnackBar(content: Text("Профиль обновлен")));
            Navigator.pop(context, true);
          }
        });
      },
    );
  }

  void showImageDialog(UpdateProfileViewModel viewModel) {
    final ImagePicker _picker = ImagePicker();
    final alert = AlertDialog(
      backgroundColor: AppColors.BACKGROUND_COLOR,
      title: Text("Выберите источник", style: TextStyle(color: Colors.black)),
      content: Text(""),
      actions: [
        TextButton(
            onPressed: () async {
              Navigator.pop(context);
              final image = await _picker.pickImage(
                  source: ImageSource.camera,
                  imageQuality: 60,
                  maxHeight: 1000,
                  maxWidth: 1000);
              if (image != null) {
                File? croppedFile = await ImageCropper().cropImage(
                  sourcePath: image.path,
                  compressQuality: 60,
                  aspectRatioPresets: [
                    CropAspectRatioPreset.square,
                  ],
                  androidUiSettings: AndroidUiSettings(
                      toolbarTitle: 'Редактирование изображений',
                      toolbarColor: Colors.red,
                      toolbarWidgetColor: Colors.white,
                      initAspectRatio: CropAspectRatioPreset.original,
                      lockAspectRatio: true),
                );
                if (croppedFile != null) {
                  setState(() {
                    imagePath1 = croppedFile.path;
                  });
                  final bytes = File(croppedFile.path).readAsBytesSync();
                  String img64 = base64Encode(bytes);
                  viewModel.updateAvatar(img64);
                }
              }
            },
            child: Text(
              "Камера",
              style: TextStyle(color: Colors.black),
            )),
        TextButton(
            onPressed: () async {
              Navigator.pop(context);
              final image = await _picker.pickImage(
                  source: ImageSource.gallery,
                  imageQuality: 60,
                  maxHeight: 1000,
                  maxWidth: 1000);
              if (image != null) {
                File? croppedFile = await ImageCropper().cropImage(
                  sourcePath: image.path,
                  compressQuality: 60,
                  aspectRatioPresets: [
                    CropAspectRatioPreset.square,
                  ],
                  androidUiSettings: AndroidUiSettings(
                      toolbarTitle: 'Редактирование изображений',
                      toolbarColor: Colors.red,
                      toolbarWidgetColor: Colors.white,
                      initAspectRatio: CropAspectRatioPreset.original,
                      lockAspectRatio: true),
                );
                if (croppedFile != null) {
                  setState(() {
                    imagePath1 = croppedFile.path;
                  });
                  final bytes = File(croppedFile.path).readAsBytesSync();
                  String img64 = base64Encode(bytes);
                  viewModel.updateAvatar(img64);
                }
              }
            },
            child: Text(
              "Галерея",
              style: TextStyle(color: Colors.black),
            )),
      ],
    );
    showDialog(context: context, builder: (_) => alert);
  }
}
