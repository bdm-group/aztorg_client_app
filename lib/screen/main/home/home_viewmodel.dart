import 'dart:async';

import 'package:aztorg_flutter/model/branch_model.dart';
import 'package:aztorg_flutter/model/category_model.dart';
import 'package:aztorg_flutter/model/offer_model.dart';
import 'package:aztorg_flutter/model/product_model.dart';
import 'package:aztorg_flutter/model/request/product_request_model.dart';
import 'package:aztorg_flutter/utils/pref_utils.dart';
import 'package:stacked/stacked.dart';

import '../../../api/api_service.dart';

class HomeViewModel extends BaseViewModel {
  final api = ApiService();

  StreamController<String> _errorStream = StreamController();

  Stream<String> get errorData {
    return _errorStream.stream;
  }

  var progressData = false;

  List<BranchModel> branchList = [];
  List<CategoryModel> primaryCategoryList = [];
  List<OfferModel> offerList = [];
  List<ProductModel> topProductList = [];

  void getUser() async {
    progressData = true;
    notifyListeners();
    final data = await api.getUser(_errorStream);
    progressData = false;
    notifyListeners();
    if (data != null) {
      PrefUtils.setToken(data.id);
      PrefUtils.setUser(data);
    }
  }

  void getBranchList() async {
    progressData = true;
    notifyListeners();
    branchList = await api.getBranchList(_errorStream);
    if (PrefUtils.getSelectedBranch() == null) {
      await PrefUtils.setSelectedBranch(branchList.first);
    }
    var selectedBranch = PrefUtils.getSelectedBranch();
    if (selectedBranch != null) {
      var items =
          branchList.where((element) => element.id == selectedBranch.id);
      if (items.length > 0) {
        items.first.checked = true;
      }
    }
    progressData = false;
    notifyListeners();
    getOfferList();
    getCategoryList();
    getProductList();
  }

  void getCategoryList() async {
    primaryCategoryList = PrefUtils.getCategoryList();
    notifyListeners();
    var items = await api.getCategoryList(_errorStream);
    await PrefUtils.setCategoryList(items);
    primaryCategoryList = PrefUtils.getCategoryList();
    notifyListeners();
  }

  void getOfferList() async {
    offerList = await api.getOfferList(_errorStream);
    notifyListeners();
  }

  void getProductList() async {
    topProductList = await api.getProductList(
        ProductRequestModel(
            PrefUtils.getSelectedBranch()?.id ?? "", "", "", "", true, []),
        _errorStream);
    notifyListeners();
  }

  @override
  void dispose() {
    _errorStream.close();
    super.dispose();
  }
}
