import 'dart:async';

import 'package:aztorg_flutter/model/request/product_request_model.dart';
import 'package:aztorg_flutter/screen/main/categories/categories_screen.dart';
import 'package:aztorg_flutter/screen/main/home/home_viewmodel.dart';
import 'package:aztorg_flutter/screen/main/product_list/product_list_screen.dart';
import 'package:aztorg_flutter/utils/app_colors.dart';
import 'package:aztorg_flutter/view/branch_item_view.dart';
import 'package:aztorg_flutter/view/category_item_view.dart';
import 'package:aztorg_flutter/view/product_item_view.dart';
import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:stacked/stacked.dart';

import '../../../utils/pref_utils.dart';
import '../../../view/offer_item_view.dart';
import 'package:easy_localization/easy_localization.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  final offerPageController = PageController();
  var currentOfferPage = 0;
  var firstTimeLoad = false;
  Timer? timer;

  @override
  void didChangeDependencies() async {
    if (firstTimeLoad) {
      firstTimeLoad = false;
    }
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<HomeViewModel>.reactive(
      viewModelBuilder: () {
        return HomeViewModel();
      },
      builder: (context, viewModel, child) => Stack(
        children: [
          Container(
            color: AppColors.BACKGROUND_COLOR,
            child: viewModel.progressData
                ? const Center(
                    child: CircularProgressIndicator(),
                  )
                : RefreshIndicator(
                    onRefresh: () async {
                      viewModel.getBranchList();
                    },
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          const SizedBox(
                            height: 16,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 16, right: 16),
                            child: Text(
                              "store".tr(),
                              style: TextStyle(
                                  fontSize: 14,
                                  fontWeight: FontWeight.bold,
                                  color: AppColors.BLACK_COLOR),
                            ),
                          ),
                          Container(
                            height: 60,
                            child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                itemCount: viewModel.branchList.length,
                                itemBuilder: (_, position) {
                                  final item = viewModel.branchList[position];
                                  return InkWell(
                                    onTap: () async {
                                      if (PrefUtils.getCartList().isEmpty) {
                                        for (var element
                                            in viewModel.branchList) {
                                          element.checked = false;
                                        }
                                        await PrefUtils.setSelectedBranch(item);
                                        viewModel.getCategoryList();
                                        viewModel.getProductList();
                                        setState(() {
                                          item.checked = true;
                                        });
                                      } else {
                                        showDialog(
                                            context: context,
                                            builder: (_) => AlertDialog(
                                                  title: Text("clear_cart_q".tr()),
                                                  content:
                                                  Text("your_really_q".tr()),
                                                  actions: [
                                                    TextButton(
                                                        onPressed: () {
                                                          PrefUtils.clearCart();
                                                          for (var element
                                                              in viewModel
                                                                  .branchList) {
                                                            element.checked =
                                                                false;
                                                          }
                                                          PrefUtils
                                                              .setSelectedBranch(
                                                                  item);
                                                          viewModel
                                                              .getCategoryList();
                                                          viewModel
                                                              .getProductList();
                                                          setState(() {
                                                            item.checked = true;
                                                          });
                                                          Navigator.of(context)
                                                              .pop();
                                                        },
                                                        child: Text("yes".tr(),
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .black))),
                                                    TextButton(
                                                        onPressed: () {
                                                          Navigator.of(context)
                                                              .pop();
                                                        },
                                                        child: Text("no".tr(),
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .black))),
                                                  ],
                                                ));
                                      }
                                    },
                                    child: BranchItemView(item: item),
                                  );
                                }),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 16, right: 16, bottom: 16),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                const SizedBox(
                                  height: 16,
                                ),
                                Container(
                                  height: 206,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(8),
                                    child: PageView(
                                      controller: offerPageController,
                                      children: viewModel.offerList
                                          .map((e) => OfferItemView(e))
                                          .toList(),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(16.0),
                                  child: Align(
                                      alignment: Alignment.center,
                                      child: SmoothPageIndicator(
                                        controller: offerPageController,
                                        count: viewModel.offerList.length,
                                        effect: SlideEffect(
                                            spacing: 8.0,
                                            radius: 4.0,
                                            dotWidth: 8.0,
                                            dotHeight: 8.0,
                                            paintStyle: PaintingStyle.fill,
                                            dotColor: AppColors.LIGHT_RED_COLOR,
                                            activeDotColor:
                                                AppColors.RED_COLOR),
                                      )),
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                        child: Text(
                                      "categories".tr(),
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          color: AppColors.BLACK_COLOR),
                                    )),
                                  ],
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                GridView.builder(
                                    shrinkWrap: true,
                                    primary: false,
                                    itemCount:
                                        viewModel.primaryCategoryList.length,
                                    gridDelegate:
                                        const SliverGridDelegateWithFixedCrossAxisCount(
                                            crossAxisCount: 2,
                                            crossAxisSpacing: 8,
                                            mainAxisSpacing: 8,
                                            childAspectRatio: 3 / 1),
                                    itemBuilder: (_, position) {
                                      final item = viewModel
                                          .primaryCategoryList[position];
                                      return InkWell(
                                        onTap: () {
                                          var subCategories =
                                              PrefUtils.getCategoryList(
                                                  parentId: item.id);
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (_) => subCategories
                                                          .isNotEmpty
                                                      ? CategoriesScreen(item)
                                                      : ProductListScreen(
                                                          ProductRequestModel(
                                                              PrefUtils.getSelectedBranch()
                                                                      ?.id ??
                                                                  "",
                                                              "",
                                                              item.id,
                                                              "",
                                                              false,
                                                              []))));
                                        },
                                        child: CategoryItemView(
                                            color: AppColors.RANDOM_COLORS[
                                                position %
                                                    AppColors
                                                        .RANDOM_COLORS.length],
                                            item: item),
                                      );
                                    }),
                                const SizedBox(
                                  height: 16,
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                        child: Text(
                                      "top_product".tr(),
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          color: AppColors.BLACK_COLOR),
                                    )),
                                  ],
                                ),
                                const SizedBox(
                                  height: 8,
                                ),
                                GridView.builder(
                                    shrinkWrap: true,
                                    primary: false,
                                    itemCount: viewModel.topProductList.length,
                                    gridDelegate:
                                        const SliverGridDelegateWithFixedCrossAxisCount(
                                            crossAxisCount: 2,
                                            crossAxisSpacing: 8,
                                            mainAxisSpacing: 8,
                                            childAspectRatio: 3 / 6),
                                    itemBuilder: (_, position) {
                                      return ProductItemView(
                                          viewModel.topProductList[position],
                                          () {
                                        //updateFavorite
                                      });
                                    }),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
          )
        ],
      ),
      onModelReady: (viewModel) {
        viewModel.errorData.listen((event) {
          ScaffoldMessenger.maybeOf(context)
              ?.showSnackBar(SnackBar(content: Text(event)));
        });
        viewModel.getBranchList();
        if(PrefUtils.getToken().isNotEmpty){
          viewModel.getUser();
        }

        timer = Timer.periodic(Duration(seconds: 4), (timer) {
          offerPageController
              .jumpToPage(currentOfferPage % viewModel.offerList.length);
          currentOfferPage++;
        });
      },
    );
  }
}
