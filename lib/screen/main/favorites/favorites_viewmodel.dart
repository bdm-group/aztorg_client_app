import 'dart:async';

import 'package:aztorg_flutter/model/product_model.dart';
import 'package:aztorg_flutter/model/request/product_request_model.dart';
import 'package:aztorg_flutter/utils/pref_utils.dart';
import 'package:stacked/stacked.dart';

import '../../../api/api_service.dart';

class FavoritesViewModel extends BaseViewModel {
  final api = ApiService();

  StreamController<String> _errorStream = StreamController();
  Stream<String> get errorData {
    return _errorStream.stream;
  }

  var progressData = false;

  List<ProductModel> favoriteProductList = [];

  void getProductList() async {
    progressData = true;
    notifyListeners();
    favoriteProductList = await api.getProductList(
        ProductRequestModel(PrefUtils.getSelectedBranch()?.id ?? "", "", "", "",
            false, PrefUtils.getFavoriteList()),
        _errorStream);
    progressData = false;
    notifyListeners();
  }

  @override
  void dispose() {
    _errorStream.close();
    super.dispose();
  }
}
