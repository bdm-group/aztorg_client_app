import 'package:aztorg_flutter/screen/main/favorites/favorites_viewmodel.dart';
import 'package:aztorg_flutter/utils/pref_utils.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import '../../../utils/app_colors.dart';
import '../../../view/product_item_view.dart';

class FavoritesScreen extends StatefulWidget {
  const FavoritesScreen({Key? key}) : super(key: key);

  @override
  _FavoritesScreenState createState() => _FavoritesScreenState();
}

class _FavoritesScreenState extends State<FavoritesScreen> {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<FavoritesViewModel>.reactive(
      viewModelBuilder: () {
        return FavoritesViewModel();
      },
      builder: (context, viewModel, child) {
        return Stack(
          children: [
            Container(
              color: AppColors.BACKGROUND_COLOR,
              child: RefreshIndicator(
                onRefresh: () async {
                  loadFavorite(viewModel);
                },
                child: Padding(
                  padding: const EdgeInsets.all(16),
                  child: viewModel.progressData
                      ? const Center(
                          child: CircularProgressIndicator(),
                        )
                      : GridView.builder(
                          itemCount: viewModel.favoriteProductList.length,
                          gridDelegate:
                              const SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 2,
                                  crossAxisSpacing: 8,
                                  mainAxisSpacing: 8,
                                  childAspectRatio: 3 / 6),
                          itemBuilder: (_, position) => ProductItemView(
                                  viewModel.favoriteProductList[position], () {
                                debugPrint("_ASS_ worked");
                              })),
                ),
              ),
            ),
            viewModel.progressData
                ? Center(child: CircularProgressIndicator())
                : Center(),
          ],
        );
      },
      onModelReady: (viewModel) {
        viewModel.errorData.listen((event) {
          ScaffoldMessenger.maybeOf(context)
              ?.showSnackBar(SnackBar(content: Text(event)));
        });

        loadFavorite(viewModel);
      },
    );
  }

  void loadFavorite(FavoritesViewModel viewModel) {
    PrefUtils.getFavoriteList().isNotEmpty
        ? viewModel.getProductList()
        : Center(child: Text("Empty"));
  }
}
