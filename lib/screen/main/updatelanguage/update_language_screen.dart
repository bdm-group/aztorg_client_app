
import 'package:aztorg_flutter/utils/app_colors.dart';
import 'package:aztorg_flutter/utils/constants.dart';
import 'package:aztorg_flutter/utils/pref_utils.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart' as GET;
import 'package:get/get_core/src/get_main.dart' as GETM;

class UpdateLanguageScreen extends StatefulWidget {
  const UpdateLanguageScreen({Key? key}) : super(key: key);

  @override
  _UpdateLanguageScreenState createState() => _UpdateLanguageScreenState();
}

class _UpdateLanguageScreenState extends State<UpdateLanguageScreen> {
  var currentLang = PrefUtils.getLang();
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            children: [
              SizedBox(
                height: 16,
              ),
              Center(
                  child: Text(
                "Выбрать язык",
                style: TextStyle(
                    fontSize: 16,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
              )),
              SizedBox(
                height: 16,
              ),
              InkWell(
                onTap: ()async{
                  await PrefUtils.setLang(RU_LANG_KEY);
                  await context.setLocale(Locale(RU_LANG_KEY));
                  await GET.Get.updateLocale(Locale(PrefUtils.getLang()));
                  Navigator.pop(context);
                  // Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (_)=>SplashScreen()), (route) => false);
                },
                child: Container(
                  height: 50,
                  decoration: BoxDecoration(
                      color: currentLang == RU_LANG_KEY ? AppColors.BLACK_COLOR : Colors.white,
                      border: Border.all(color: AppColors.GREY_LIGHT_COLOR),
                      borderRadius: BorderRadius.circular(24)),
                  padding:
                      EdgeInsets.only(left: 16, right: 16, top: 4, bottom: 4),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                          child: Text(
                        "Русский",
                        style: TextStyle(
                          color: currentLang == RU_LANG_KEY ? Colors.white : Colors.black,
                          fontSize: 16,
                        ),
                      )),
                      Icon(currentLang == RU_LANG_KEY ? Icons.radio_button_checked_rounded : Icons.radio_button_off_rounded, color: currentLang == RU_LANG_KEY ? Colors.white : Colors.black,),
                      // Image.asset(
                      //   "assets/images/ru.png",
                      //   width: 28,
                      //   height: 28,
                      // ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 16,
              ),
              InkWell(
                onTap: ()async{
                  context.locale = (Locale(KZ_LANG_KEY));
                  await PrefUtils.setLang(KZ_LANG_KEY);
                  await GET.Get.updateLocale(Locale(PrefUtils.getLang()));
                  Navigator.pop(context);
                  // Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (_)=>MainScreen()), (route) => false);
                },
                child: Container(
                  height: 50,
                  decoration: BoxDecoration(
                      color: currentLang == KZ_LANG_KEY ? AppColors.BLACK_COLOR : Colors.white,
                      border: Border.all(color: AppColors.GREY_LIGHT_COLOR),
                      borderRadius: BorderRadius.circular(24)),
                  padding:
                      EdgeInsets.only(left: 16, right: 16, top: 4, bottom: 4),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                          child: Text(
                        "Қазақ",
                        style: TextStyle(
                          color: currentLang == KZ_LANG_KEY ? Colors.white : Colors.black,
                          fontSize: 16,
                        ),
                      )),
                      Icon(currentLang == KZ_LANG_KEY ? Icons.radio_button_checked_rounded : Icons.radio_button_off_rounded, color: currentLang == KZ_LANG_KEY ? Colors.white : Colors.black,),
                      // Image.asset(
                      //   "assets/images/kz.png",
                      //   width: 28,
                      //   height: 28,
                      // ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
