import 'package:aztorg_flutter/utils/pref_utils.dart';
import 'package:aztorg_flutter/view/category_circle_item_view.dart';
import 'package:flutter/material.dart';

import '../../../model/category_model.dart';
import '../../../utils/app_colors.dart';
import 'package:easy_localization/easy_localization.dart';

class CategoriesScreen extends StatefulWidget {
  CategoryModel? item;
  CategoriesScreen(this.item,{Key? key}) : super(key: key);

  @override
  _CategoriesScreenState createState() => _CategoriesScreenState();
}

class _CategoriesScreenState extends State<CategoriesScreen> {
  @override
  Widget build(BuildContext context) {
    var items = widget.item == null ? PrefUtils.getCategoryList() : PrefUtils.getCategoryList(parentId: widget.item!.id);
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.item?.name ?? "categories".tr()),
        ),
        body: Container(
            child: GridView.builder(
              itemCount: items.length,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3, childAspectRatio: 3 / 4),
          itemBuilder: (_, position) => CategoryCircleItemView(
              color: AppColors
                  .RANDOM_COLORS[position % AppColors.RANDOM_COLORS.length],
              item: items[position]),
        )));
  }
}
