import 'package:aztorg_flutter/model/request/product_request_model.dart';
import 'package:aztorg_flutter/screen/auth/auth_screen.dart';
import 'package:aztorg_flutter/screen/main/cart/cart_screen.dart';
import 'package:aztorg_flutter/screen/main/favorites/favorites_screen.dart';
import 'package:aztorg_flutter/screen/main/home/home_screen.dart';
import 'package:aztorg_flutter/screen/main/product_list/product_list_screen.dart';
import 'package:aztorg_flutter/utils/app_colors.dart';
import 'package:aztorg_flutter/utils/pref_utils.dart';
import 'package:aztorg_flutter/view/bottom_navigation_item.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

import 'account/account_screen.dart';
import 'orders/orders_screen.dart';

class MainScreen extends StatefulWidget {
  final int selectedIndex;

  MainScreen({this.selectedIndex = 0, Key? key}) : super(key: key);
  final screenItems = [
    BottomNavigationItem("main".tr(),
        ImageIcon(AssetImage("assets/images/home.png")), HomeScreen()),
    BottomNavigationItem(
        "favorites".tr(),
        ImageIcon(AssetImage("assets/images/favorites.png")),
        FavoritesScreen()),
    BottomNavigationItem("cart".tr(),
        ImageIcon(AssetImage("assets/images/cart.png")), CartScreen()),
    BottomNavigationItem("orders".tr(),
        ImageIcon(AssetImage("assets/images/orders.png")), OrdersScreen()),
    BottomNavigationItem("profile".tr(),
        ImageIcon(AssetImage("assets/images/profile.png")), AccountScreen()),
  ];

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  var selectedScreenIndex = 0;
  var firstTimeRun = true;

  Future<bool> _onWillPop() async {
    return (await showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: const Text("Осторожность"),
            content: const Text("Вы действительно хотите выйти из программы??"),
            actions: <Widget>[
              TextButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: const Text("нет", style: TextStyle(color: Colors.red)),
              ),
              TextButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: const Text("да", style: TextStyle(color: Colors.blue)),
              ),
            ],
          ),
        )) ??
        false;
  }

  @override
  void didChangeDependencies() {
    if (firstTimeRun) {
      updateLang();
      selectedScreenIndex = widget.selectedIndex;
      getLocation();
      firstTimeRun = false;
    }
    super.didChangeDependencies();
  }

  void updateLang()async{
    context.locale = (Locale(PrefUtils.getLang()));
  }

  void getLocation() async {
    try{
      await Geolocator.requestPermission();
    }catch(e){

    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: false,
          elevation: 1,
          title: Image.asset(
            "assets/images/logo.png",
            height: 30,
          ),
          actions: [
            // IconButton(
            //     onPressed: () {},
            //     icon: SvgPicture.asset(
            //       "assets/images/nofication_svg.svg",
            //       height: 24,
            //       width: 24,
            //     )),

            Visibility(
              visible: selectedScreenIndex < 2,
              child: IconButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (_) => ProductListScreen(
                                  ProductRequestModel(
                                      PrefUtils.getSelectedBranch()?.id ?? "",
                                      "",
                                      "",
                                      "",
                                      false, []),
                                  showSearchbar: true,
                                )));
                  },
                  icon: Icon(
                    Icons.search,
                    size: 24,
                    color: AppColors.RED_COLOR,
                  )),
            ),
          ],
        ),
        backgroundColor: AppColors.BLACK_COLOR,
        body: ClipRRect(
          borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(20),
              bottomRight: Radius.circular(20)),
          child: Container(
            height: double.infinity,
            width: double.infinity,
            child: widget.screenItems[selectedScreenIndex].screen,
          ),
        ),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          backgroundColor: AppColors.BLACK_COLOR,
          selectedItemColor: AppColors.YELLOW_COLOR,
          selectedIconTheme: IconThemeData(color: AppColors.YELLOW_COLOR),
          unselectedIconTheme: IconThemeData(color: Colors.white),
          unselectedItemColor: Colors.white,
          elevation: 0,
          items: widget.screenItems
              .map((item) =>
                  BottomNavigationBarItem(icon: item.icon, label: item.title))
              .toList(),
          currentIndex: selectedScreenIndex,
          onTap: (index) {
            switch (index) {
              case 0:
                break;
              case 1:
                break;
              case 2:
                if (PrefUtils.getToken().isEmpty) {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (_) => AuthScreen()));
                  return;
                }
                break;
              case 3:
                if (PrefUtils.getToken().isEmpty) {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (_) => AuthScreen()));
                  return;
                }
                break;
              case 4:
                if (PrefUtils.getToken().isEmpty) {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (_) => AuthScreen()));
                  return;
                }
                break;
            }

            setState(() {
              selectedScreenIndex = index;
            });
          },
        ),
      ),
    );
  }

  void getPermission() {}
}
