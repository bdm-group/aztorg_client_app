import 'dart:async';

import 'package:aztorg_flutter/model/request/product_request_model.dart';
import 'package:aztorg_flutter/screen/main/cartinfo/cart_info_view.dart';
import 'package:aztorg_flutter/screen/main/product_list/product_list_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import '../../../utils/app_colors.dart';
import '../../../view/product_item_view.dart';
import 'package:easy_localization/easy_localization.dart';

class ProductListScreen extends StatefulWidget {
  ProductRequestModel filter;
  bool showSearchbar;

  ProductListScreen(this.filter, {this.showSearchbar = false, Key? key}) : super(key: key);

  @override
  _ProductListScreenState createState() => _ProductListScreenState();
}

class _ProductListScreenState extends State<ProductListScreen> {
  var showSearchBar = false;
  var firstTimeRun = true;
  FocusNode inputNode = FocusNode();

  @override
  void didChangeDependencies() {
    if(firstTimeRun){
      showSearchBar = widget.showSearchbar;
      firstTimeRun = false;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<ProductListViewModel>.reactive(
      viewModelBuilder: () {
        return ProductListViewModel();
      },
      builder: (context, viewModel, child) {
        return Scaffold(
          appBar: AppBar(
            title: showSearchBar
                ? TextField(
                    focusNode: inputNode,
                    autofocus: true,
                    onChanged: (text) {
                      widget.filter.keyword = text;
                      if(text.length > 2 || widget.filter.category_id.isNotEmpty){
                        viewModel.getProductList(widget.filter);
                      }else{
                        viewModel.productList = [];
                        viewModel.notifyListeners();
                      }
                    },
                    decoration: InputDecoration(
                        hintText: "input_name_product_".tr(),
                        border: InputBorder.none),
                  )
                : Text("product_list".tr()),
            actions: [
              IconButton(
                  onPressed: () {
                    setState(() {
                      showSearchBar = !showSearchBar;
                      if (!showSearchBar) {
                        widget.filter.keyword = "";
                        viewModel.productList = [];
                        viewModel.notifyListeners();
                        if(widget.filter.category_id.isNotEmpty){
                          viewModel.getProductList(widget.filter);
                        }
                      }
                    });
                    if (showSearchBar) {
                      Timer(Duration(seconds: 1), () {
                        FocusScope.of(context).requestFocus(inputNode);
                      });
                    }
                  },
                  icon:
                      Icon(showSearchBar ? Icons.close_rounded : Icons.search)),
            ],
          ),
          body: Stack(
            children: [
              Container(
                color: AppColors.BACKGROUND_COLOR,
                child: RefreshIndicator(
                  onRefresh: () async {
                    viewModel.getProductList(widget.filter);
                  },
                  child: Container(
                    child: viewModel.progressData
                        ? const Center(
                            child: CircularProgressIndicator(),
                          )
                        : viewModel.productList.isEmpty ? Center(child: Text("list_empty".tr())) :  GridView.builder(
                            itemCount: viewModel.productList.length,
                            gridDelegate:
                                const SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 2,
                                    crossAxisSpacing: 8,
                                    mainAxisSpacing: 8,
                                    childAspectRatio: 3 / 6),
                            itemBuilder: (_, position) {
                              return ProductItemView(
                                  viewModel.productList[position], () {
                                //updateFavorite
                              });
                            }),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: CartInfoView(),
              )
            ],
          ),
        );
      },
      onModelReady: (viewModel) {
        if(widget.filter.category_id.isNotEmpty){
          viewModel.getProductList(widget.filter);
        }
        viewModel.errorData.listen((event) {
          ScaffoldMessenger.maybeOf(context)
              ?.showSnackBar(SnackBar(content: Text(event)));
        });

        // viewModel.getProductList(widget.filter);
      },
    );
  }
}
