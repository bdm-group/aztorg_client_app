import 'dart:async';

import 'package:async/async.dart';
import 'package:aztorg_flutter/model/product_model.dart';
import 'package:aztorg_flutter/model/request/product_request_model.dart';
import 'package:stacked/stacked.dart';

import '../../../api/api_service.dart';
import '../../../utils/pref_utils.dart';

class ProductListViewModel extends BaseViewModel {
  final api = ApiService();

  StreamController<String> _errorStream = StreamController();
  Stream<String> get errorData {
    return _errorStream.stream;
  }

  var progressData = false;
  var cartAmount = 0.0;
  var keyword = "";

  List<ProductModel> productList = [];

  void getProductList(ProductRequestModel request) async {
    progressData = true;
    notifyListeners();
    productList = await api.getProductList(request, _errorStream);
    progressData = false;
    notifyListeners();
  }

  void getCartProductList() async {
    notifyListeners();
    var cartProductList = await api.getProductList(
        ProductRequestModel(
          PrefUtils.getSelectedBranch()?.id ?? "",
          "",
          "",
          "",
          false,
          List.from(PrefUtils.getCartList().map((item) => item.id)),
        ),
        _errorStream);
    cartAmount = 0.0;

    notifyListeners();
  }

  @override
  void dispose() {
    _errorStream.close();
    super.dispose();
  }
}
