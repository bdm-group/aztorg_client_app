import 'dart:async';

import 'package:aztorg_flutter/model/branch_model.dart';
import 'package:aztorg_flutter/model/category_model.dart';
import 'package:aztorg_flutter/model/offer_model.dart';
import 'package:aztorg_flutter/model/product_model.dart';
import 'package:aztorg_flutter/model/request/product_request_model.dart';
import 'package:aztorg_flutter/model/response/login_response.dart';
import 'package:aztorg_flutter/model/response/report_response.dart';
import 'package:aztorg_flutter/utils/pref_utils.dart';
import 'package:stacked/stacked.dart';

import '../../../api/api_service.dart';

class ReportViewModel extends BaseViewModel {
  final api = ApiService();

  StreamController<String> _errorStream = StreamController();
  Stream<String> get errorData {
    return _errorStream.stream;
  }

  ReportResponse? report;

  var progressData = false;

  void getReport(String startDate, String endDate) async {
    progressData = true;
    notifyListeners();
    report = await api.getReport(startDate, endDate, _errorStream);
    progressData = false;
    notifyListeners();
  }

  @override
  void dispose() {
    _errorStream.close();
    super.dispose();
  }
}
