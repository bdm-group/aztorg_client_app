import 'package:aztorg_flutter/screen/main/orders/orders_viewmodel.dart';
import 'package:aztorg_flutter/utils/app_colors.dart';
import 'package:aztorg_flutter/view/order_item_layout.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class OrdersScreen extends StatefulWidget {
  const OrdersScreen({Key? key}) : super(key: key);

  @override
  State<OrdersScreen> createState() => _OrdersScreenState();
}

class _OrdersScreenState extends State<OrdersScreen> {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<OrdersViewModel>.reactive(
      builder:
          (BuildContext context, OrdersViewModel viewModel, Widget? child) {
        return Stack(
          children: [
            Container(
              color: AppColors.BACKGROUND_COLOR,
              child: RefreshIndicator(
                onRefresh: () async {
                  viewModel.getOrderList();
                },
                child: viewModel.progressData
                    ? const Center(
                        child: CircularProgressIndicator(),
                      )
                    : ListView.builder(
                        itemCount: viewModel.items.length,
                        itemBuilder: (_, position) {
                          var item = viewModel.items[position];
                          return OrderItemLayout(item);
                        }),
              ),
            ),
            viewModel.progressData
                ? Center(child: CircularProgressIndicator())
                : Center(),
          ],
        );
      },
      viewModelBuilder: () {
        return OrdersViewModel();
      },
      onModelReady: (viewModel) {
        viewModel.errorData.listen((event) {
          ScaffoldMessenger.maybeOf(context)
              ?.showSnackBar(SnackBar(content: Text(event)));
        });
        viewModel.getOrderList();
      },
    );
  }
}
