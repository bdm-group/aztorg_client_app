import 'dart:async';

import 'package:aztorg_flutter/model/order_model.dart';
import 'package:stacked/stacked.dart';

import '../../../api/api_service.dart';

class OrdersViewModel extends BaseViewModel {
  final api = ApiService();

  StreamController<String> _errorStream = StreamController();

  Stream<String> get errorData {
    return _errorStream.stream;
  }

  var progressData = false;

  List<OrderModel> items = [];

  void getOrderList() async {
    progressData = true;
    notifyListeners();
    items = await api.getOrderList(_errorStream);
    progressData = false;
    notifyListeners();
  }

  @override
  void dispose() {
    _errorStream.close();
    super.dispose();
  }
}
