import 'dart:async';

import 'package:aztorg_flutter/model/branch_model.dart';
import 'package:aztorg_flutter/model/category_model.dart';
import 'package:aztorg_flutter/model/offer_model.dart';
import 'package:aztorg_flutter/model/product_model.dart';
import 'package:aztorg_flutter/model/request/product_request_model.dart';
import 'package:aztorg_flutter/model/response/login_response.dart';
import 'package:aztorg_flutter/utils/pref_utils.dart';
import 'package:stacked/stacked.dart';

import '../../../api/api_service.dart';

class AccountViewModel extends BaseViewModel {
  final api = ApiService();

  StreamController<String> _errorStream = StreamController();
  Stream<String> get errorData {
    return _errorStream.stream;
  }

  StreamController<LoginResponse> _userStream = StreamController();
  Stream<LoginResponse> get userData {
    return _userStream.stream;
  }

  var progressData = false;

  void getUser() async {
    progressData = true;
    notifyListeners();
    final data = await api.getUser(_errorStream);
    progressData = false;
    notifyListeners();
    if (data != null) {
      _userStream.sink.add(data);
      PrefUtils.setToken(data.id);
      PrefUtils.setUser(data);
    }
    notifyListeners();
  }

  @override
  void dispose() {
    _errorStream.close();
    super.dispose();
  }
}
