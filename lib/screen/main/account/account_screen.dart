import 'package:aztorg_flutter/extensions/extensions.dart';
import 'package:aztorg_flutter/screen/main/account/account_viewmodel.dart';
import 'package:aztorg_flutter/screen/main/htmlview/h_t_m_l_view_screen.dart';
import 'package:aztorg_flutter/screen/main/report/report_screen.dart';
import 'package:aztorg_flutter/screen/main/updateprofile/update_profile_screen.dart';
import 'package:aztorg_flutter/screen/splash/splash_screen.dart';
import 'package:aztorg_flutter/utils/constants.dart';
import 'package:aztorg_flutter/utils/pref_utils.dart';
import 'package:aztorg_flutter/view/custom_views.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import '../../../utils/app_colors.dart';
import '../updatelanguage/update_language_screen.dart';

class AccountScreen extends StatefulWidget {
  const AccountScreen({Key? key}) : super(key: key);

  @override
  State<AccountScreen> createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<AccountViewModel>.reactive(
      viewModelBuilder: () => AccountViewModel(),
      builder:
          (BuildContext context, AccountViewModel viewModel, Widget? child) {
        return Container(
            color: AppColors.BACKGROUND_COLOR,
            child: SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(40),
                          child: Container(
                            color: Colors.grey,
                            height: 80,
                            width: 80,
                            child: CustomViews.buildNetworkImageWithoutRadius(
                                PrefUtils.getUser().image),
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  PrefUtils.getUser().name,
                                  style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  height: 2,
                                ),
                                Text(
                                  PrefUtils.getUser().phone,
                                  style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.black,
                                  ),
                                ),
                                OutlinedButton(
                                    style: ButtonStyle(
                                      shape: MaterialStateProperty.all(
                                        RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(30.0),
                                        ),
                                      ),
                                      side: MaterialStateProperty.all(
                                        BorderSide(
                                            width: 2,
                                            color: AppColors.RED_COLOR),
                                      ),
                                    ),
                                    onPressed: () async {
                                      var r = await showModalBottomSheet(
                                          context: context,
                                          isScrollControlled: true,
                                          builder: (_) =>
                                              UpdateProfileScreen());
                                      if (r == true) {
                                        viewModel.getUser();
                                      }
                                    },
                                    child: Text(
                                      "change_profile".tr(),
                                      style: TextStyle(color: Colors.black),
                                    )),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    Card(
                      child: Padding(
                        padding: const EdgeInsets.all(8),
                        child: Column(
                          children: [
                            SizedBox(
                              height: 8,
                            ),
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (_) => ReportScreen()));
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  children: [
                                    Expanded(
                                        child: Text(
                                      "Акт сверки",
                                      style: TextStyle(
                                          fontSize: 16, color: Colors.black),
                                    )),
                                    Icon(
                                      Icons.keyboard_arrow_right_rounded,
                                      color: Colors.grey,
                                    )
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (_) => HTMLViewScreen(
                                            "cashback".tr(),
                                            "Правила зачисления кэшбека")));
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  children: [
                                    Expanded(
                                        child: Text(
                                      "cashback_balance".tr(),
                                      style: TextStyle(
                                          fontSize: 16, color: Colors.black),
                                    )),
                                    Text(
                                      PrefUtils.getUser()
                                          .keshback
                                          .formattedAmountString(),
                                      style: TextStyle(
                                          fontSize: 16, color: Colors.grey),
                                    ),
                                    Icon(
                                      Icons.keyboard_arrow_right_rounded,
                                      color: Colors.grey,
                                    )
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            InkWell(
                              onTap: () {
                                showModalBottomSheet(
                                    context: context,
                                    builder: (_) => UpdateLanguageScreen());
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  children: [
                                    Expanded(
                                        child: Text(
                                      "language".tr(),
                                      style: TextStyle(
                                          fontSize: 16, color: Colors.black),
                                    )),
                                    Text(
                                      PrefUtils.getLang() == KZ_LANG_KEY
                                          ? "Қазақ"
                                          : "Русский",
                                      style: TextStyle(
                                          fontSize: 16, color: Colors.grey),
                                    ),
                                    Icon(
                                      Icons.keyboard_arrow_right_rounded,
                                      color: Colors.grey,
                                    )
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (_) => HTMLViewScreen(
                                            "about_us".tr(),
                                            PrefUtils.getUser().about)));
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  children: [
                                    Expanded(
                                        child: Text(
                                      "about_us".tr(),
                                      style: TextStyle(
                                          fontSize: 16, color: Colors.black),
                                    )),
                                    Icon(
                                      Icons.info_outline_rounded,
                                      color: Colors.grey,
                                    )
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (_) => HTMLViewScreen(
                                            "privacy_policy".tr(),
                                            PrefUtils.getUser().policies)));
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  children: [
                                    Expanded(
                                        child: Text(
                                      "privacy_policy".tr(),
                                      style: TextStyle(
                                          fontSize: 16, color: Colors.black),
                                    )),
                                    Icon(
                                      Icons.privacy_tip_outlined,
                                      color: Colors.grey,
                                    )
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            InkWell(
                              onTap: () {
                                showDialog(
                                    context: context,
                                    builder: (builder) {
                                      return AlertDialog(
                                        title: Text("logout".tr()),
                                        content: Text("you_really_logout".tr()),
                                        actions: [
                                          TextButton(
                                              onPressed: () {
                                                PrefUtils.clearAll();
                                                Navigator.pushAndRemoveUntil(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (_) =>
                                                            SplashScreen()),
                                                    (route) => false);
                                              },
                                              child: Text(
                                                "yes".tr(),
                                                style: TextStyle(
                                                    color: AppColors.RED_COLOR),
                                              )),
                                          TextButton(
                                              onPressed: () {
                                                Navigator.pop(context);
                                              },
                                              child: Text(
                                                "no".tr(),
                                                style: TextStyle(
                                                    color: AppColors.RED_COLOR),
                                              )),
                                        ],
                                      );
                                    });
                              },
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  children: [
                                    Expanded(
                                        child: Text(
                                      "logout".tr(),
                                      style: TextStyle(
                                          fontSize: 16, color: Colors.red),
                                    )),
                                    Icon(
                                      Icons.logout,
                                      color: Colors.grey,
                                    )
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ));
      },
      onModelReady: (viewModel) {
        viewModel.userData.listen((event) {});

        viewModel.getUser();
      },
    );
  }
}
