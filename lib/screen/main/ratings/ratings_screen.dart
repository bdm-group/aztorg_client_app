import 'dart:io';

import 'package:aztorg_flutter/model/rating_data_model.dart';
import 'package:aztorg_flutter/view/rating_item_view.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

import '../../../utils/app_colors.dart';
import 'package:easy_localization/easy_localization.dart';

class RatingsScreen extends StatefulWidget {
  RatingDataModel data;

  RatingsScreen(this.data, {Key? key}) : super(key: key);

  @override
  _RatingsScreenState createState() => _RatingsScreenState();
}

class _RatingsScreenState extends State<RatingsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("reviews".tr()),
        ),
        backgroundColor: Colors.white,
        body: Container(
            child: ListView.builder(
                itemCount: widget.data.reviews.length,
                itemBuilder: (_, position) => RatingItemView(widget.data.reviews[position]))));
  }
}
