import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:aztorg_flutter/model/branch_model.dart';
import 'package:aztorg_flutter/model/category_model.dart';
import 'package:aztorg_flutter/model/offer_model.dart';
import 'package:aztorg_flutter/model/product_model.dart';
import 'package:aztorg_flutter/model/rating_data_model.dart';
import 'package:aztorg_flutter/model/request/product_request_model.dart';
import 'package:aztorg_flutter/model/response/login_response.dart';
import 'package:aztorg_flutter/model/response/report_response.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

import '../main.dart';
import '../model/base_model.dart';
import '../model/make_order_model.dart';
import '../model/order_model.dart';
import '../utils/pref_utils.dart';

class ApiService {
  final dio = Dio();

  ApiService() {
    dio.options.baseUrl = PrefUtils.getBaseUrl();

    dio.options.headers.addAll({
      'Authorization': 'Basic ' +
          base64Encode(utf8.encode(
              '${PrefUtils.getBdmItems()?.mobile_username ?? ""}:${PrefUtils.getBdmItems()?.mobile_password ?? ""}')),
      'token': PrefUtils.getToken(),
      'device': Platform.operatingSystem,
      'lang': PrefUtils.getLang(),
    });
    dio.interceptors.add(MyApp.alice.getDioInterceptor());
  }

  BaseModel wrapResponse(Response response) {
    if (response.statusCode == 200) {
      final data = BaseModel.fromJson(response.data);
      if (data.success) {
        return data;
      } else {
        if (data.error_code == 1) {
          PrefUtils.setToken("");
        }
      }
      return data;
    } else {
      return BaseModel(
          false,
          response.statusMessage ?? "Unknown error ${response.statusCode}",
          -1,
          null);
    }
  }

  String wrapError(DioError error) {
    if (kDebugMode) {
      return error.message;
    }
    switch (error.type) {
      case DioErrorType.other:
        return "Network error.";
      case DioErrorType.cancel:
        return "Unknown error.";
      case DioErrorType.connectTimeout:
        return "Unknown error.";
      case DioErrorType.receiveTimeout:
        return "Unknown error.";
      case DioErrorType.sendTimeout:
        return "Unknown error.";
      case DioErrorType.response:
        return "Unknown error.";
    }
    return error.message;
  }

  Future<bool?> checkPhone(
      String phone, StreamController<String> errorStream) async {
    try {
      final response =
          await dio.post("check-phone", data: jsonEncode({"phone": phone}));
      final baseData = wrapResponse(response);
      if (baseData.success) {
        return baseData.data["isRegistered"];
      } else {
        errorStream.sink.add(baseData.message ?? "Error");
      }
    } catch (e) {
      errorStream.sink.add(e.toString());
    }
    return null;
  }

  Future<LoginResponse?> regestry(String phone, String name, String sms,
      StreamController<String> errorStream) async {
    try {
      final response = await dio.post("regestry",
          data: jsonEncode({
            "phone": phone,
            "name": name,
            "sms": sms,
            "lat": "",
            "long": "",
            "adress": "",
            "image": ""
          }));

      final baseData = wrapResponse(response);
      if (baseData.success) {
        return LoginResponse.fromJson(baseData.data);
      } else {
        errorStream.sink.add(baseData.message ?? "Error");
      }
    } catch (e) {
      errorStream.sink.add(e.toString());
    }
    return null;
  }

  Future<String?> registration(
      String phone,
      String code,
      String ownername,
      int category_id,
      int region_id,
      int districts_id,
      double latitude,
      double longitude,
      String comment,
      StreamController<String> errorStream) async {
    try {
      final response = await dio.post("registration",
          data: jsonEncode({
            "phone": phone,
            "code": code,
            "ownername": ownername,
            "category_id": category_id,
            "region_id": region_id,
            "districts_id": districts_id,
            "latitude": latitude,
            "longitude": longitude,
            "comment": comment,
          }));
      final baseData = wrapResponse(response);
      if (baseData.success) {
        return baseData.data["token"];
      } else {
        errorStream.sink.add(baseData.message ?? "Error");
      }
    } catch (e) {
      errorStream.sink.add(e.toString());
    }
    return null;
  }

  Future<List<BranchModel>> getBranchList(
      StreamController<String> errorStream) async {
    try {
      final response = await dio.get("getBranches");
      final baseData = wrapResponse(response);
      if (baseData.success) {
        return (baseData.data as List<dynamic>)
            .map((json) => BranchModel.fromJson(json))
            .toList();
      } else {
        errorStream.sink.add(baseData.message ?? "Error");
      }
    } on DioError catch (e) {
      errorStream.sink.add(wrapError(e));
    }
    return [];
  }

  Future<List<CategoryModel>> getCategoryList(
      StreamController<String> errorStream) async {
    try {
      final response = await dio.get("category", queryParameters: {
        "branchId": PrefUtils.getSelectedBranch()?.id ?? ""
      });
      final baseData = wrapResponse(response);
      if (baseData.success) {
        return (baseData.data as List<dynamic>)
            .map((json) => CategoryModel.fromJson(json))
            .toList();
      } else {
        errorStream.sink.add(baseData.message ?? "Error");
      }
    } on DioError catch (e) {
      errorStream.sink.add(wrapError(e));
    }
    return [];
  }

  Future<List<OfferModel>> getOfferList(
      StreamController<String> errorStream) async {
    try {
      final response = await dio.get("offer", queryParameters: {
        "branchId": PrefUtils.getSelectedBranch()?.id ?? ""
      });
      final baseData = wrapResponse(response);
      if (baseData.success) {
        return (baseData.data as List<dynamic>)
            .map((json) => OfferModel.fromJson(json))
            .toList();
      } else {
        errorStream.sink.add(baseData.message ?? "Error");
      }
    } on DioError catch (e) {
      errorStream.sink.add(wrapError(e));
    }
    return [];
  }

  Future<List<ProductModel>> getProductList(
      ProductRequestModel filter, StreamController<String> errorStream) async {
    try {
      final response = await dio.post("product",
          data: filter.toJson(),
          queryParameters: {
            "branchId": PrefUtils.getSelectedBranch()?.id ?? ""
          });
      final baseData = wrapResponse(response);
      if (baseData.success) {
        var items = (baseData.data as List<dynamic>)
            .map((json) => ProductModel.fromJson(json))
            .toList();
        var favorites = PrefUtils.getFavoriteList();
        items.forEach((item) {
          item.favorite =
              favorites.where((element) => item.id == element).isNotEmpty;
        });
        return items;
      } else {
        errorStream.sink.add(baseData.message ?? "Error");
      }
    } on DioError catch (e) {
      errorStream.sink.add(wrapError(e));
    }
    return [];
  }

  Future<RatingDataModel?> getRatingData(
      String product_id, StreamController<String> errorStream) async {
    try {
      final response = await dio.get("rating", queryParameters: {
        "product_id": product_id,
        "branchId": PrefUtils.getSelectedBranch()?.id ?? ""
      });
      final baseData = wrapResponse(response);
      if (baseData.success) {
        return RatingDataModel.fromJson(baseData.data);
      } else {
        errorStream.sink.add(baseData.message ?? "Error");
      }
    } on DioError catch (e) {
      errorStream.sink.add(wrapError(e));
    }
    return null;
  }

  Future<bool?> smsCheck(
      String phone, StreamController<String> errorStream) async {
    try {
      final response =
          await dio.get("SmsCheck", queryParameters: {"phone": phone});
      final baseData = wrapResponse(response);
      if (baseData.success) {
        return baseData.data["isRegistered"];
      } else {
        errorStream.sink.add(baseData.message ?? "Error");
      }
    } catch (e) {
      errorStream.sink.add(e.toString());
    }
    return null;
  }

  Future<int?> makeOrder(
      MakeOrderModel orderModel, StreamController<String> errorStream) async {
    try {
      final response =
      await dio.post("makeOrder", data: jsonEncode(orderModel));
      final baseData = wrapResponse(response);
      if (baseData.success) {
        return baseData.data["number"];
      } else {
        errorStream.sink.add(baseData.message ?? "Error");
      }
    } catch (e) {
      errorStream.sink.add(e.toString());
    }
    return null;
  }


  Future<bool> updateAvatar(String image, StreamController<String> errorStream) async {
    try {
      final response = await dio.post("postImage", data: jsonEncode({
        "imagename": image,
        "name": DateTime.now().millisecondsSinceEpoch.toString() + ".jpg"
      }));
      final baseData = wrapResponse(response);
      if (baseData.success) {
        return true;
      } else {
        errorStream.sink.add(baseData.message ?? "Error");
      }
    } catch (e) {
      errorStream.sink.add(e.toString());
    }
    return false;
  }

  Future<bool> updateProfile(String fullname, String phone, String lat, String long, StreamController<String> errorStream) async {
    try {
      final response = await dio.post("editProfile", data: jsonEncode({
        "fullname": fullname,
        "phone": phone,
        "lat": lat,
        "long": long,
      }));
      final baseData = wrapResponse(response);
      if (baseData.success) {
        return true;
      } else {
        errorStream.sink.add(baseData.message ?? "Error");
      }
    } catch (e) {
      errorStream.sink.add(e.toString());
    }
    return false;
  }

  Future<int?> postRating(double rating, String description, String id,
      StreamController<String> errorStream) async {
    try {
      final response = await dio.post("postRating",
          data: jsonEncode(
              {"rating": rating, "description": description, "id": id}));
      final baseData = wrapResponse(response);
      if (baseData.success) {
        return baseData.data["number"];
      } else {
        errorStream.sink.add(baseData.message ?? "Error");
      }
    } catch (e) {
      errorStream.sink.add(e.toString());
    }
    return null;
  }



  Future<List<OrderModel>> getOrderList(
      StreamController<String> errorStream) async {
    try {
      final response = await dio.get("orderlist", queryParameters: {
        "branchId": PrefUtils.getSelectedBranch()?.id ?? ""
      });
      final baseData = wrapResponse(response);
      if (baseData.success) {
        return (baseData.data as List<dynamic>)
            .map((json) => OrderModel.fromJson(json))
            .toList();
      } else {
        errorStream.sink.add(baseData.message ?? "Error");
      }
    } on DioError catch (e) {
      errorStream.sink.add(wrapError(e));
    }
    return [];
  }

  Future<LoginResponse?> getUser(StreamController<String> errorStream) async {
    try {
      final response = await dio.get("getUser", queryParameters: {
        "branchId": PrefUtils.getSelectedBranch()?.id ?? ""
      });

      final baseData = wrapResponse(response);
      if (baseData.success) {
        return LoginResponse.fromJson(baseData.data);
      } else {
        errorStream.sink.add(baseData.message ?? "Error");
      }
    } catch (e) {
      errorStream.sink.add(e.toString());
    }
    return null;
  }

  Future<ReportResponse?> getReport(String startDate, String endDate, StreamController<String> errorStream) async {
    try {
      final response = await dio.get("clientSverka", queryParameters: {
        "branchId": PrefUtils.getSelectedBranch()?.id ?? "",
        "bsana": startDate,
        "osana": endDate,
        "currencyId": "e798ee0a-411f-11ec-b18c-e8039add9cfa",
      });

      final baseData = wrapResponse(response);
      print("baseData ${baseData.data}");
      if (baseData.success) {
        print("baseData from ${baseData.data}");
        return ReportResponse.fromJson(baseData.data);
      } else {
        errorStream.sink.add(baseData.message ?? "Error");
      }
    } catch (e) {
      errorStream.sink.add(e.toString());
    }
    return null;
  }
}
