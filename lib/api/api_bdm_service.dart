import 'dart:async';
import 'dart:io';

import 'package:aztorg_flutter/model/base_bdm_model.dart';
import 'package:aztorg_flutter/model/bdm_items_model.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

import '../main.dart';
import '../utils/constants.dart';
import '../utils/pref_utils.dart';

class ApiBdmService {
  final dio = Dio();

  ApiBdmService() {
    dio.options.baseUrl = BASE_BDM_URL;

    dio.options.headers.addAll(
        {'token': PrefUtils.getToken(), 'device': Platform.operatingSystem});
    dio.interceptors.add(MyApp.alice.getDioInterceptor());
  }

  BaseBdmModel wrapResponse(Response response) {
    if (response.statusCode == 200) {
      final data = BaseBdmModel.fromJson(response.data);
      return data;
    } else {
      return BaseBdmModel(
          true,
          response.statusMessage ?? "Unknown error ${response.statusCode}",
          null);
    }
  }

  String wrapError(DioError error){
    if(kDebugMode){
      return error.message;
    }
    switch(error.type){
      case DioErrorType.other:
        return "Network error.";
      case DioErrorType.cancel:
        return "Unknown error.";
      case DioErrorType.connectTimeout:
        return "Unknown error.";
      case DioErrorType.receiveTimeout:
        return "Unknown error.";
      case DioErrorType.sendTimeout:
        return "Unknown error.";
      case DioErrorType.response:
        return "Unknown error.";
    }
    return error.message;
  }

  Future<BdmItemsModel?> loadConfig(StreamController<String> errorStream) async {
    try {
      final response = await dio.get(SECRET_NAME);
      final baseData = wrapResponse(response);
      if (!baseData.error) {
        return BdmItemsModel.fromJson(baseData.items);
      } else {
        errorStream.sink.add(baseData.message ?? "Error");
      }
    } on DioError catch (e) {
      errorStream.sink.add(wrapError(e));
    }
    return null;
  }

}
