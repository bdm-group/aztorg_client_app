import 'package:aztorg_flutter/view/custom_views.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import '../model/rating_model.dart';
import '../utils/app_colors.dart';
import 'package:aztorg_flutter/extensions/extensions.dart';

class RatingItemView extends StatelessWidget {
  RatingModel item;
  RatingItemView(this.item,{Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ListTile(
            leading: ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: Container(
                color: Colors.grey,
                height: 40,
                width: 40,
                child: CustomViews.buildNetworkImage(
                    item.user_avatar),
              ),
            ),
            title: Text(
              item.user_fullaname,
              style: TextStyle(
                  fontSize: 16,
                  color: AppColors.BLACK_COLOR,
                  fontWeight: FontWeight.bold),
            ),
            subtitle: RatingBar.builder(
              initialRating: item.rating,
              minRating: 1,
              itemCount: 5,
              itemSize: 20,
              unratedColor: AppColors.GREY_LIGHT_COLOR,
              itemBuilder: (context, _) => Icon(
                Icons.star,
                color: Colors.amber,
              ),
              onRatingUpdate: (rating) {
                print(rating);
              },
            ),
          ),
          Text(
            item.comment,
            style: TextStyle(
                fontSize: 14, color: AppColors.GREY_TEXT_COLOR),
          ),
          SizedBox(
            height: 8,
          ),
          // Container(
          //   height: 76,
          //   child: ListView.builder(
          //       itemCount: item.images.length,
          //       shrinkWrap: true,
          //       scrollDirection: Axis.horizontal,
          //       itemBuilder: (_, position) {
          //         return Container(
          //           margin: EdgeInsets.all(4),
          //           height: 70,
          //           width: 70,
          //           decoration: BoxDecoration(
          //               color: Colors.white,
          //               borderRadius: BorderRadius.circular(8),
          //               border: Border.all(
          //                   color: AppColors.GREY_LIGHT_COLOR)),
          //           child: CustomViews.buildNetworkImage(
          //             item.images[position],
          //             width: double.infinity,
          //             height: double.infinity,
          //           ),
          //         );
          //       }),
          // ),
          SizedBox(
            height: 8,
          ),
          Text(
            item.date_time.formattedDateTime,
            style: TextStyle(
                fontSize: 12, color: AppColors.GREY_TEXT_COLOR),
          ),
          SizedBox(
            height: 8,
          ),
          Divider(),
        ],
      ),
    );
  }
}
