import 'package:aztorg_flutter/extensions/extensions.dart';
import 'package:aztorg_flutter/model/product_model.dart';
import 'package:aztorg_flutter/screen/main/product_detail/product_detail_screen.dart';
import 'package:aztorg_flutter/utils/app_colors.dart';
import 'package:aztorg_flutter/view/custom_views.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import '../utils/pref_utils.dart';

class ProductItemView extends StatefulWidget {
  ProductModel item;
  Function updateFavorite;

  ProductItemView(this.item, this.updateFavorite, {Key? key})
      : super(key: key);

  @override
  State<ProductItemView> createState() => _ProductItemViewState();
}

class _ProductItemViewState extends State<ProductItemView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (_) => ProductDetailScreen(widget.item)));
      },
      child: Container(
        margin: EdgeInsets.only(left: 4, right: 4, top: 4),
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: AppColors.GREY_LIGHT_COLOR, width: 1.5),
            borderRadius: BorderRadius.circular(5)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
                child: CustomViews.buildNetworkImage(
              widget.item.images.isNotEmpty ? widget.item.images.first : "",
              width: double.infinity,
            )),
            Padding(
              padding: const EdgeInsets.all(8),
              child: InkWell(
                onTap: () {
                  widget.updateFavorite;
                },
                child: Text(
                  widget.item.name,
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.bold,
                      color: AppColors.BLACK_COLOR),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Align(
                alignment: Alignment.topLeft,
                child: RatingBar.builder(
                  initialRating: widget.item.rating,
                  minRating: 1,
                  itemCount: 5,
                  itemSize: 20,
                  ignoreGestures: true,
                  unratedColor: AppColors.GREY_LIGHT_COLOR,
                  itemBuilder: (context, _) => const Icon(
                    Icons.star,
                    color: Colors.amber,
                  ), onRatingUpdate: (double value) {  },
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8, left: 8, right: 8),
              child: Text(
                widget.item.price.formattedAmountString(),
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                    color: AppColors.BLACK_COLOR),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 8, right: 8, bottom: 8),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        if (widget.item.old_price > 0)
                          Text(
                            widget.item.old_price.formattedAmountString(),
                            style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.bold,
                                decorationColor: AppColors.GREY_TEXT_COLOR,
                                decorationStyle: TextDecorationStyle.solid,
                                decoration: TextDecoration.lineThrough,
                                color: AppColors.GREY_TEXT_COLOR),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                        if (widget.item.old_price > 0)
                          const SizedBox(
                            width: 8,
                          ),
                        if (widget.item.old_price > 0)
                          Text(
                            "${(widget.item.old_price - widget.item.price) / 0.01 * widget.item.price}% Off",
                            style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.bold,
                                color: AppColors.RED_COLOR),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                      ],
                    ),
                  ),
                  IconButton(
                      onPressed: () {
                        setState(() {
                          PrefUtils.setFavoriteProduct(widget.item);
                          widget.item.favorite = !widget.item.favorite;
                        });
                        widget.updateFavorite;
                        debugPrint("ASS_ Pressed");
                      },
                      iconSize: 24,
                      icon: Icon(
                        widget.item.favorite
                            ? Icons.favorite_rounded
                            : Icons.favorite_border,
                        color: widget.item.favorite
                            ? AppColors.RED_COLOR
                            : AppColors.GREY_COLOR,
                      ))
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
