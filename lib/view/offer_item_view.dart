import 'package:aztorg_flutter/model/offer_model.dart';
import 'package:aztorg_flutter/view/custom_views.dart';
import 'package:flutter/material.dart';

class OfferItemView extends StatelessWidget {
  final OfferModel item;
  const OfferItemView(this.item,{Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child:  Stack(
        children: [
          CustomViews.buildNetworkImage(
            item.image,
            fit: BoxFit.cover,
            width: double.infinity,
            height: double.infinity,
          ),
          ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(8)),
            child: Container(color: Colors.black.withAlpha(40),),
          ),
          Align(
            alignment: Alignment.bottomLeft,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(
                item.title,
                style: TextStyle(
                  fontSize: 24,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
