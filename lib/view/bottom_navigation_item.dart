

import 'package:flutter/material.dart';

class BottomNavigationItem{
  final String title;
  final Widget icon;
  final Widget screen;

  BottomNavigationItem(this.title, this.icon, this.screen);
}