import 'package:aztorg_flutter/model/product_model.dart';
import 'package:aztorg_flutter/utils/pref_utils.dart';
import 'package:aztorg_flutter/view/custom_views.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../model/basket_model.dart';
import '../utils/app_colors.dart';
import 'package:aztorg_flutter/extensions/extensions.dart';

class CartItemLayout extends StatefulWidget {
  ProductModel item;
  Function updateCart;
  Function deleteCart;
  CartItemLayout(this.item, this.updateCart, this.deleteCart, {Key? key}) : super(key: key);

  @override
  State<CartItemLayout> createState() => _CartItemLayoutState();
}

class _CartItemLayoutState extends State<CartItemLayout> {
  var cartCount = 0.0;
  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    cartCount = PrefUtils.getCartCount(widget.item.id);
    return Container(
      margin: const EdgeInsets.only(left: 8, right: 8, top: 4, bottom: 4),
      padding: EdgeInsets.all(8),
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: AppColors.GREY_LIGHT_COLOR),
          borderRadius: BorderRadius.circular(5)),
      child: Row(
        children: [
          // Image.asset("assets/images/product1.png", width: 80, height: 80,),
          Expanded(
            flex: 1,
            child: CustomViews.buildNetworkImage(widget.item.images.first, width: 80, height: 80,),
          ),
          SizedBox(width: 4,),
          Expanded(
            flex: 3,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Row(
                  children: [
                    Expanded(
                        child: Text(
                      widget.item.name,
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: AppColors.BLACK_COLOR),
                    )),
                    IconButton(
                        onPressed: () {
                          setState(() {
                            PrefUtils.setFavoriteProduct(widget.item);
                            widget.item.favorite = !widget.item.favorite;
                          });
                        },
                        icon: Icon(
                          widget.item.favorite ? Icons.favorite_rounded : Icons.favorite_border,
                          color: widget.item.favorite ? AppColors.RED_COLOR : AppColors.GREY_COLOR,
                          size: 24,
                        )),
                    InkWell(
                        child: Image.asset(
                          "assets/images/trash.png",
                          width: 24,
                          height: 24,
                        ),
                        onTap: () {
                          PrefUtils.addToCart(
                              BasketModel(widget.item.id, 0, 0.0, ""));
                          widget.deleteCart();
                        }),
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                        child: Text(
                      widget.item.price.formattedAmountString(),
                      style: const TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: Colors.black),
                    )),
                    Container(
                      decoration: BoxDecoration(
                          color: AppColors.GREY_LIGHT_COLOR,
                          borderRadius: BorderRadius.circular(8),
                          border: Border.all(
                              color: AppColors.GREY_LIGHT_COLOR, width: 1)),
                      child: Row(
                        children: [
                          InkWell(
                            onTap: () {
                              setState(() {
                                cartCount--;
                              });
                              if (cartCount > 0) {
                                PrefUtils.addToCart(BasketModel(
                                    widget.item.id,
                                    cartCount,
                                    widget.item.price,
                                    widget.item.category_id));
                              } else {
                                PrefUtils.addToCart(
                                    BasketModel(widget.item.id, 0, 0.0, ""));
                                cartCount = 0.0;
                                widget.deleteCart();
                              }
                              widget.updateCart();
                            },
                            child: Container(
                              padding: const EdgeInsets.only(
                                  left: 16, right: 16, top: 2, bottom: 2),
                              decoration: const BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(8),
                                    bottomLeft: Radius.circular(8)),
                              ),
                              child: Center(
                                  child: Text(
                                "-",
                                style: TextStyle(
                                    color: AppColors.GREY_TEXT_COLOR,
                                    fontSize: 20),
                              )),
                            ),
                          ),
                          Center(
                              child: Padding(
                            padding: const EdgeInsets.only(left: 8, right: 8),
                            child: Row(
                              children: [
                                InkWell(
                                    child: Text(
                                      cartCount.toString(),
                                      style: TextStyle(
                                          color: AppColors.GREY_TEXT_COLOR,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 16),
                                    ),
                                    onTap: () {
                                      debugPrint("ASS_ " +
                                          PrefUtils.getCartList()
                                              .first
                                              .count
                                              .toString());
                                    }),
                                SizedBox(
                                  width: 4,
                                ),
                                Text(
                                  widget.item.unity,
                                  style: TextStyle(
                                      color: AppColors.GREY_TEXT_COLOR,
                                      fontSize: 12),
                                ),
                              ],
                            ),
                          )),
                          InkWell(
                            onTap: () {
                              setState(() {
                                cartCount++;
                                widget.updateCart();
                              });
                              PrefUtils.addToCart(BasketModel(
                                  widget.item.id,
                                  cartCount,
                                  widget.item.price,
                                  widget.item.category_id));
                              widget.updateCart();
                            },
                            child: Container(
                              padding: const EdgeInsets.only(
                                  left: 16, right: 16, top: 2, bottom: 2),
                              decoration: const BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                    topRight: Radius.circular(8),
                                    bottomRight: Radius.circular(8)),
                              ),
                              child: Center(
                                  child: Text(
                                "+",
                                style: TextStyle(
                                    color: AppColors.GREY_TEXT_COLOR,
                                    fontSize: 20),
                              )),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
