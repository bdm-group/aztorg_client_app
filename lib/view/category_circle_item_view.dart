import 'package:aztorg_flutter/model/category_model.dart';
import 'package:aztorg_flutter/utils/app_colors.dart';
import 'package:aztorg_flutter/view/custom_views.dart';
import 'package:flutter/material.dart';

import '../model/request/product_request_model.dart';
import '../screen/main/categories/categories_screen.dart';
import '../screen/main/product_list/product_list_screen.dart';
import '../utils/pref_utils.dart';

class CategoryCircleItemView extends StatelessWidget {
  final Color color;
  final CategoryModel item;

  const CategoryCircleItemView(
      {Key? key, required this.color, required this.item})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        var subCategories = PrefUtils.getCategoryList(parentId: item.id);
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (_) => subCategories.length > 0
                    ? CategoriesScreen(item)
                    : ProductListScreen(ProductRequestModel(
                        PrefUtils.getSelectedBranch()?.id ?? "",
                        "",
                        item.id,
                        "",
                        false, []))));
      },
      child: Container(
        margin: EdgeInsets.only(left: 8, right: 8, top: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 100,
              width: 100,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  border: Border.all(color: color),
                  color: color.withAlpha(20)),
              child: Center(
                child: CustomViews.buildNetworkImage(
                  item.icon.isEmpty
                      ? PrefUtils.getParentCategoryList(item.category_id).isNotEmpty
                          ? PrefUtils.getParentCategoryList(item.category_id)
                              .first
                              .icon
                          : ""
                      : item.icon,
                  width: 70,
                  height: 70,
                ),
              ),
            ),
            SizedBox(
              height: 8,
            ),
            Text(
              item.name,
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: AppColors.BLACK_COLOR,
                  fontSize: 18,
                  fontWeight: FontWeight.bold),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            )
          ],
        ),
      ),
    );
  }
}
