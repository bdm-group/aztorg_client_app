import 'package:aztorg_flutter/model/branch_model.dart';
import 'package:aztorg_flutter/model/category_model.dart';
import 'package:aztorg_flutter/utils/app_colors.dart';
import 'package:flutter/material.dart';

import '../screen/main/categories/categories_screen.dart';

class BranchItemView extends StatelessWidget {
  final BranchModel item;

  const BranchItemView({Key? key, required this.item})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(8),
      padding: EdgeInsets.only(left: 16, right: 16),
      decoration: BoxDecoration(
        color: item.checked ? AppColors.RED_COLOR : null,
          border: Border.all(color: AppColors.RED_COLOR),
          borderRadius: BorderRadius.circular(20)),
      child: Center(
        child: Text(
          item.name,
          style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: item.checked ? Colors.white : AppColors.BLACK_COLOR),
        ),
      ),
    );
  }
}
