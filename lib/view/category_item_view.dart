import 'package:aztorg_flutter/model/category_model.dart';
import 'package:aztorg_flutter/utils/app_colors.dart';
import 'package:aztorg_flutter/view/custom_views.dart';
import 'package:flutter/material.dart';

import '../screen/main/categories/categories_screen.dart';

class CategoryItemView extends StatelessWidget {
  final Color color;
  final CategoryModel item;

  const CategoryItemView({Key? key, required this.color, required this.item})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8),
      decoration: BoxDecoration(
        color: color.withAlpha(20),
          border: Border.all(color: color),
          borderRadius: BorderRadius.circular(10)),
      child: Row(
        children: [
          CustomViews.buildNetworkImage(item.icon,
            width: 40,
            height: 40,),
          SizedBox(width: 8,),
          Expanded(
            child: Text(
              item.name,
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: AppColors.BLACK_COLOR),
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          )
        ],
      ),
    );
  }
}
