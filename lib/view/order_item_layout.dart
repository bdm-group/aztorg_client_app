import 'package:aztorg_flutter/extensions/extensions.dart';
import 'package:aztorg_flutter/model/order_model.dart';
import 'package:aztorg_flutter/utils/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

class OrderItemLayout extends StatelessWidget {
  OrderModel item;

  OrderItemLayout(this.item, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 8, left: 8, right: 8, bottom: 0),
      child: Card(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 16, right: 16, top: 16),
              child: Row(
                children: [
                  Expanded(
                      child: Text(
                    "${"order".tr()} №${item.id}",
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: AppColors.RED_COLOR),
                  )),
                  Text(
                    DateTime.parse(item.date).formattedDateTime,
                    style: TextStyle(
                        fontSize: 14, color: AppColors.GREY_TEXT_COLOR),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 16, right: 8),
              child: Row(
                children: [
                  Icon(
                    Icons.airport_shuttle_rounded,
                    color: Colors.grey.shade500,
                    size: 20,
                  ),
                  SizedBox(
                    width: 4,
                  ),
                  Text(
                    item.statusName,
                    style: TextStyle(
                        fontSize: 16, color: AppColors.GREY_TEXT_COLOR),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16),
              child: ListView.builder(
                  shrinkWrap: true,
                  primary: false,
                  itemCount: item.order_product.length,
                  itemBuilder: (_, position) {
                    var product = item.order_product[position];
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: Text(
                                product.name,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    fontSize: 12, color: Colors.black, fontWeight: FontWeight.bold),
                              ), flex: 4,),
                            SizedBox(width: 4,),
                            Expanded(
                              child: Text(
                                product.count.toString() + " " + product.unity,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    fontSize: 12, color: AppColors.GREY_TEXT_COLOR),
                              ), flex: 2,),
                            SizedBox(width: 4,),
                            Expanded(
                              child: Text(
                                product.total_amount.formattedAmountString(),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    fontSize: 12, color: AppColors.GREY_TEXT_COLOR),
                              ), flex: 2,),
                          ],
                        ),
                        Divider(),
                      ],
                    );
                  }),
            ),
            Divider(
              height: 1,
            ),
            Container(
              color: AppColors.BACKGROUND_COLOR.withAlpha(200),
              padding: EdgeInsets.all(8),
              child: Row(
                children: [
                  Expanded(
                      child: Text(""),),
                  Text(
                    "${"total_amount_".tr()} ",
                    style: TextStyle(
                        fontSize: 14, color: AppColors.GREY_TEXT_COLOR),
                  ),
                  Text(
                    item.summa.formattedAmountString(),
                    style: TextStyle(
                        fontSize: 16, fontWeight: FontWeight.bold, color: AppColors.BLACK_COLOR),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
