
class TestModel {
  final String name;
  final double count;
  final double price;

  TestModel({
    required this.name,
    required this.count,
    required this.price,
  });
}