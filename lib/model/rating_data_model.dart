
import 'package:aztorg_flutter/model/rating_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:json_annotation/json_annotation.dart';

part 'rating_data_model.g.dart';

@JsonSerializable()
class RatingDataModel extends ChangeNotifier {
  final double average_amount;
  final List<RatingModel> reviews;

  RatingDataModel(this.average_amount, this.reviews);

  factory RatingDataModel.fromJson(Map<String, dynamic> json) => _$RatingDataModelFromJson(json);

  Map<String, dynamic> toJson() => _$RatingDataModelToJson(this);
}
