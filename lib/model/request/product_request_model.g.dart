// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_request_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductRequestModel _$ProductRequestModelFromJson(Map<String, dynamic> json) {
  return ProductRequestModel(
    json['branchId'] as String,
    json['keyword'] as String,
    json['category_id'] as String,
    json['sort'] as String,
    json['only_top_products'] as bool,
    (json['product_ids'] as List<dynamic>).map((e) => e as String).toList(),
  );
}

Map<String, dynamic> _$ProductRequestModelToJson(
        ProductRequestModel instance) =>
    <String, dynamic>{
      'branchId': instance.branchId,
      'keyword': instance.keyword,
      'category_id': instance.category_id,
      'sort': instance.sort,
      'only_top_products': instance.only_top_products,
      'product_ids': instance.product_ids,
    };
