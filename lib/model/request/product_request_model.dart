import 'package:json_annotation/json_annotation.dart';

part 'product_request_model.g.dart';

@JsonSerializable()
class ProductRequestModel {
  final String branchId;
  String keyword;
  final String category_id;
  final String sort;
  final bool only_top_products;
  final List<String> product_ids;

  ProductRequestModel(this.branchId, this.keyword, this.category_id, this.sort,
      this.only_top_products, this.product_ids);

  factory ProductRequestModel.fromJson(Map<String, dynamic> json) =>
      _$ProductRequestModelFromJson(json);

  Map<String, dynamic> toJson() => _$ProductRequestModelToJson(this);
}
