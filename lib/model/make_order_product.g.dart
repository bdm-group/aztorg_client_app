// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'make_order_product.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MakeOrderProduct _$MakeOrderProductFromJson(Map<String, dynamic> json) {
  return MakeOrderProduct(
    (json['count'] as num).toDouble(),
    (json['price'] as num).toDouble(),
    json['store_id'] as String,
    json['barcode'] as String,
    json['product_id'] as String,
    (json['summa'] as num).toDouble(),
  );
}

Map<String, dynamic> _$MakeOrderProductToJson(MakeOrderProduct instance) =>
    <String, dynamic>{
      'count': instance.count,
      'price': instance.price,
      'store_id': instance.store_id,
      'barcode': instance.barcode,
      'product_id': instance.product_id,
      'summa': instance.summa,
    };
