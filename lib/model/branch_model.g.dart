// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'branch_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BranchModel _$BranchModelFromJson(Map<String, dynamic> json) {
  return BranchModel(
    json['id'] as String,
    json['name'] as String,
    (json['kod'] as num).toDouble(),
    json['nameTypePrice'] as String,
    json['idTypePrice'] as String,
    json['currency'] as String,
  );
}

Map<String, dynamic> _$BranchModelToJson(BranchModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'kod': instance.kod,
      'nameTypePrice': instance.nameTypePrice,
      'idTypePrice': instance.idTypePrice,
      'currency': instance.currency,
    };
