
import 'package:json_annotation/json_annotation.dart';

part 'rating_model.g.dart';

@JsonSerializable()
class RatingModel {
  final String user_fullaname;
  final String user_avatar;
  final double rating;
  final String comment;
  final String date_time;
  final List<String> images;

  RatingModel(this.user_fullaname, this.user_avatar, this.rating, this.comment, this.date_time, this.images);

  factory RatingModel.fromJson(Map<String, dynamic> json) => _$RatingModelFromJson(json);

  Map<String, dynamic> toJson() => _$RatingModelToJson(this);
}
