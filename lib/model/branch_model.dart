

import 'package:json_annotation/json_annotation.dart';

part 'branch_model.g.dart';

@JsonSerializable()
class BranchModel{
  final String id;
  final String name;
  final double kod;
  final String nameTypePrice;
  final String idTypePrice;
  final String currency;



  @JsonKey(ignore: true)
  var checked = false;
  BranchModel(this.id, this.name,this.kod,this.nameTypePrice,this.idTypePrice,this.currency);

  factory BranchModel.fromJson(Map<String, dynamic> json) => _$BranchModelFromJson(json);

  Map<String, dynamic> toJson() => _$BranchModelToJson(this);
}