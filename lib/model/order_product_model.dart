import 'package:json_annotation/json_annotation.dart';

import 'make_order_product.dart';

part 'order_product_model.g.dart';

@JsonSerializable()
class OrderProductModel {
  final String branchId;
  final String name;
  final String unity;
  final double count;
  final double price;
  final double total_amount;

  OrderProductModel(this.branchId, this.name, this.unity, this.count, this.price, this.total_amount);

  factory OrderProductModel.fromJson(Map<String, dynamic> json) =>
      _$OrderProductModelFromJson(json);

  Map<String, dynamic> toJson() => _$OrderProductModelToJson(this);
}


