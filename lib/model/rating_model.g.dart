// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rating_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RatingModel _$RatingModelFromJson(Map<String, dynamic> json) {
  return RatingModel(
    json['user_fullaname'] as String,
    json['user_avatar'] as String,
    (json['rating'] as num).toDouble(),
    json['comment'] as String,
    json['date_time'] as String,
    (json['images'] as List<dynamic>).map((e) => e as String).toList(),
  );
}

Map<String, dynamic> _$RatingModelToJson(RatingModel instance) =>
    <String, dynamic>{
      'user_fullaname': instance.user_fullaname,
      'user_avatar': instance.user_avatar,
      'rating': instance.rating,
      'comment': instance.comment,
      'date_time': instance.date_time,
      'images': instance.images,
    };
