import 'package:json_annotation/json_annotation.dart';

part 'product_model.g.dart';

@JsonSerializable()
class ProductModel {
  final String id;
  final String name;
  final String category;
  final String category_id;
  final double price;
  final double old_price;
  final String storeName;
  final String store_id;
  final String unity;
  final double rating;
  final List<String> images;
  final String description;
  final String barcode;
  @JsonKey(ignore: true)
  var favorite = false;

  ProductModel(
      this.id,
      this.name,
      this.category,
      this.category_id,
      this.price,
      this.old_price,
      this.storeName,
      this.store_id,
      this.unity,
      this.rating,
      this.images,
      this.description,
      this.barcode);

  factory ProductModel.fromJson(Map<String, dynamic> json) =>
      _$ProductModelFromJson(json);

  Map<String, dynamic> toJson() => _$ProductModelToJson(this);
}
