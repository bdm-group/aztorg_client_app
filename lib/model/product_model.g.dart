// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductModel _$ProductModelFromJson(Map<String, dynamic> json) {
  return ProductModel(
    json['id'] as String,
    json['name'] as String,
    json['category'] as String,
    json['category_id'] as String,
    (json['price'] as num).toDouble(),
    (json['old_price'] as num).toDouble(),
    json['storeName'] as String,
    json['store_id'] as String,
    json['unity'] as String,
    (json['rating'] as num).toDouble(),
    (json['images'] as List<dynamic>).map((e) => e as String).toList(),
    json['description'] as String,
    json['barcode'] as String,
  );
}

Map<String, dynamic> _$ProductModelToJson(ProductModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'category': instance.category,
      'category_id': instance.category_id,
      'price': instance.price,
      'old_price': instance.old_price,
      'storeName': instance.storeName,
      'store_id': instance.store_id,
      'unity': instance.unity,
      'rating': instance.rating,
      'images': instance.images,
      'description': instance.description,
      'barcode': instance.barcode,
    };
