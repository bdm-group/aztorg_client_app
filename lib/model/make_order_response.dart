class MakeOrderResponse {
  final int number;
  MakeOrderResponse(this.number);

  factory MakeOrderResponse.fromJson(dynamic json) =>
      MakeOrderResponse(json["number"]);
}
