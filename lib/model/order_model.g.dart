// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderModel _$OrderModelFromJson(Map<String, dynamic> json) {
  return OrderModel(
    json['id'] as String,
    json['idDoc'] as String,
    json['date'] as String,
    (json['total_count'] as num).toDouble(),
    (json['summa'] as num).toDouble(),
    json['status'] as int,
    json['statusName'] as String,
    (json['order_product'] as List<dynamic>)
        .map((e) => OrderProductModel.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$OrderModelToJson(OrderModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'idDoc': instance.idDoc,
      'date': instance.date,
      'total_count': instance.total_count,
      'summa': instance.summa,
      'status': instance.status,
      'statusName': instance.statusName,
      'order_product': instance.order_product,
    };
