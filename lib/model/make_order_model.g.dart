// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'make_order_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MakeOrderModel _$MakeOrderModelFromJson(Map<String, dynamic> json) {
  return MakeOrderModel(
    json['branchId'] as String,
    json['comment'] as String,
    (json['products'] as List<dynamic>)
        .map((e) => MakeOrderProduct.fromJson(e as Map<String, dynamic>))
        .toList(),
    json['soboy'] as bool,
    json['lat'] as String,
    json['long'] as String,
    (json['keshback'] as num).toDouble(),
  );
}

Map<String, dynamic> _$MakeOrderModelToJson(MakeOrderModel instance) =>
    <String, dynamic>{
      'branchId': instance.branchId,
      'comment': instance.comment,
      'soboy': instance.soboy,
      'lat': instance.lat,
      'long': instance.long,
      'keshback': instance.keshback,
      'products': instance.products,
    };
