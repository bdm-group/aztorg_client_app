import 'package:json_annotation/json_annotation.dart';

import 'make_order_product.dart';

part 'make_order_model.g.dart';

@JsonSerializable()
class MakeOrderModel {
  final String branchId;
  String comment;
  bool soboy;
  String lat;
  String long;
  double keshback;
  final List<MakeOrderProduct> products;

  MakeOrderModel(this.branchId, this.comment, this.products, this.soboy, this.lat, this.long, this.keshback);

  factory MakeOrderModel.fromJson(Map<String, dynamic> json) =>
      _$MakeOrderModelFromJson(json);

  Map<String, dynamic> toJson() => _$MakeOrderModelToJson(this);
}


