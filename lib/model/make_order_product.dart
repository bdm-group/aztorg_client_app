import 'package:json_annotation/json_annotation.dart';
part 'make_order_product.g.dart';

@JsonSerializable()
class MakeOrderProduct {
  final double count;
  final double price;
  final String store_id;
  final String barcode;
  final String product_id;
  final double summa;

  MakeOrderProduct(this.count, this.price, this.store_id, this.barcode,
      this.product_id, this.summa);

  factory MakeOrderProduct.fromJson(Map<String, dynamic> json) =>
      _$MakeOrderProductFromJson(json);

  Map<String, dynamic> toJson() => _$MakeOrderProductToJson(this);
}