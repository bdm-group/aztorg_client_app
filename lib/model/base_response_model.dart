

class BaseResponseModel<T>{
  final bool success;
  final String message;
  final int error_code;
  final T data;

  BaseResponseModel(this.success, this.message, this.error_code, this.data);
}