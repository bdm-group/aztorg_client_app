
import 'package:aztorg_flutter/model/order_product_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'order_model.g.dart';

@JsonSerializable()
class OrderModel {
  final String id;
  final String idDoc;
  final String date;
  final double total_count;
  final double summa;
  final int status;
  final String statusName;
  final List<OrderProductModel> order_product;
  OrderModel(this.id, this.idDoc, this.date, this.total_count, this.summa, this.status, this.statusName, this.order_product);

  factory OrderModel.fromJson(Map<String, dynamic> json) => _$OrderModelFromJson(json);

  Map<String, dynamic> toJson() => _$OrderModelToJson(this);
}
