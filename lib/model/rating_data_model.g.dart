// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rating_data_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RatingDataModel _$RatingDataModelFromJson(Map<String, dynamic> json) {
  return RatingDataModel(
    (json['average_amount'] as num).toDouble(),
    (json['reviews'] as List<dynamic>)
        .map((e) => RatingModel.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$RatingDataModelToJson(RatingDataModel instance) =>
    <String, dynamic>{
      'average_amount': instance.average_amount,
      'reviews': instance.reviews,
    };
