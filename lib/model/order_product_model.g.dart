// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'order_product_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OrderProductModel _$OrderProductModelFromJson(Map<String, dynamic> json) {
  return OrderProductModel(
    json['branchId'] as String,
    json['name'] as String,
    json['unity'] as String,
    (json['count'] as num).toDouble(),
    (json['price'] as num).toDouble(),
    (json['total_amount'] as num).toDouble(),
  );
}

Map<String, dynamic> _$OrderProductModelToJson(OrderProductModel instance) =>
    <String, dynamic>{
      'branchId': instance.branchId,
      'name': instance.name,
      'unity': instance.unity,
      'count': instance.count,
      'price': instance.price,
      'total_amount': instance.total_amount,
    };
