package uz.bdm.aztorg.aztorg_flutter
import io.flutter.plugins.firebase.messaging.FlutterFirebaseMessagingBackgroundService
import android.app.Application
import io.flutter.plugin.common.PluginRegistry
import io.flutter.plugins.GeneratedPluginRegistrant
import io.flutter.embedding.engine.FlutterEngine;
import android.util.Log
import uz.bdm.aztorg.aztorg_flutter.AppSignatureHelper


class MyApp(): Application(), PluginRegistry.PluginRegistrantCallback {
  override
  fun onCreate() {
    super.onCreate()
//    FlutterFirebaseMessagingBackgroundService.setPluginRegistrant(this)
     Log.d("JW", AppSignatureHelper(this).appSignatures[0])
  }

  override
  fun registerWith(registry: PluginRegistry) {
    GeneratedPluginRegistrant.registerWith(FlutterEngine(applicationContext))
  }
}